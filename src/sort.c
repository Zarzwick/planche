// A selection sort from rosetta code, just to be sure.
void selection_sort(
	// int *a, int n
	void* base,
	size_t n,
	size_t size,
	int (*compare)(const void*, const void*, void*),
	void* arg
) {
	if (size != 4) {
		int a = 0 / 0 ;
	}
	int* a = (int*) base ;
    int i, j, m, t ;
    for (i = 0; i < n; i++) {
        for (j = i, m = i; j < n; j++) {
        	if (compare(&a[j], &a[m], arg)) {
                m = j ;
            }
        }
        t = a[i] ;
        a[i] = a[m] ;
        a[m] = t ;
    }
}
