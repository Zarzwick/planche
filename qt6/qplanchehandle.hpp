#pragma once

#include <QAbstractButton>

///
/// ...
///
class QPlancheHandle: public QAbstractButton {
	Q_OBJECT

public:
	explicit QPlancheHandle(QWidget* parent=nullptr) ;
	virtual ~QPlancheHandle() {}

    QSize sizeHint() const override ;
    QSize minimumSizeHint() const override ;

protected:
	bool event(QEvent *e) override;
    void paintEvent(QPaintEvent *) override;
    void keyPressEvent(QKeyEvent *) override;
    void mouseMoveEvent(QMouseEvent *) override;
    bool hitButton(const QPoint &pos) const override;
} ;
