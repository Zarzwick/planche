#include "planche.c"
#include <stdio.h>

int int_max(int a, int b) { return a < b ? b : a ; }
int int_min(int a, int b) { return a < b ? a : b ; }

#define eval(TITLE, CMD) \
	if (! (CMD)) fprintf(stderr, "- test \"%s\" failed\n", TITLE) ;

#define expect(PREDICATE, MSG) \
	if (! (PREDICATE)) { fprintf(stderr, "%s\n", MSG) ; return false ; }

bool test_vec() {
	double d ;
	vec_t v = vec_init_capacity(sizeof(double), 4) ;
	d = 10.0 ; vec_append(&v, &d) ;
	d = 10.1 ; vec_append(&v, &d) ;
	d = 10.2 ; vec_append(&v, &d) ;
	d = 10.3 ; vec_append(&v, &d) ;
	d = 10.4 ; vec_append(&v, &d) ;
	const char* msg = "wrong value" ;
	expect(vec_get(&v, 0, double) == 10.0, msg) ;
	expect(vec_get(&v, 1, double) == 10.1, msg) ;
	expect(vec_get(&v, 2, double) == 10.2, msg) ;
	expect(vec_get(&v, 3, double) == 10.3, msg) ;
	expect(vec_get(&v, 4, double) == 10.4, msg) ;
	d = 203.490 ; vec_set(&v, 3, &d) ;
	expect(vec_get(&v, 4, double) == 10.4, msg) ;
	expect(vec_get(&v, 3, double) == 203.490, msg) ;
	vec_remove(&v, 2) ;
	expect(vec_get(&v, 2, double) == 203.490, msg) ;
	expect(v.items.len == 4, msg) ;
	vec_free(&v) ;
	return true ;
}

bool test_vec_resize() {
	vec_t v = vec_init_capacity(sizeof(int), 4) ;
	for (int i = 0; i < 1000; i++) {
		vec_append(&v, &i) ;
	}
	expect(v.items.len == 1000, "wrong length") ;
	expect(v.capacity == 1024, "wrong growth/capacity") ;
	vec_free(&v) ;
	return true ;
}

int vec_sort_cmp(const void* a, const void* b, void* useless) {
	return *((int*)a) < *((int*)b) ;
}

bool test_vec_sort() {
	vec_t v = vec_init_capacity(sizeof(int), 4) ;
	for (int i = 0; i < 513; i++) {
		vec_append(&v, &i) ;
	}
	qsort_r(v.items.ptr, v.items.len, v.itemsize, vec_sort_cmp, NULL) ;
	return true ;
}

void pretty_print(plyt_t* plyt, FILE* file) {
	// Print cells.
	for (int i = 0; i < plyt->cells.items.len; ++ i) {
		rect2fp_t cell = vec_get(&plyt->cells, i, rect2fp_t) ;
		fprintf(
			file, "Cell %d: pos (%f, %f), size (%f, %f)\n", i,
			to_unit_float(cell.pos[0] ), to_unit_float(cell.pos[1] ),
			to_unit_float(cell.size[0]), to_unit_float(cell.size[1])
		) ;
		rect2u_t size = vec_get(&plyt->cells_sizes, i, rect2u_t) ;
		fprintf(
			file, "\t> size: pos (%d, %d), size (%d, %d)\n",
			size.pos[0] , size.pos[1],
			size.size[0], size.size[1]
		) ;
	}
	for (int axis = 0; axis < 2; ++ axis) {
		for (int j = 0; j < plyt->junctions[axis].items.len; ++ j) {
			junction_t junc = vec_get(&plyt->junctions[axis], j, junction_t) ;
			fprintf(
				file, "Junctions %d (axis #%d): a %d, b %d, p %f, s %f\n",
				j, axis, junc.a, junc.b,
				to_unit_float(junc.p), to_unit_float(junc.s)
			) ;
		}
	}
}

void render_rect(
	plyt_t* plyt,
	int* buffer,
	int ox, int oy,
	int sx, int sy,
	int col
) {
	for (int i = 0; i < sy; ++ i)
		for (int j = 0; j < sx; ++ j)
			buffer[(oy+i)*plyt->size[H]+(ox+j)] = col ;
}

void render(plyt_t* plyt, const char* filename) { // to .pgm
	const size_t n = plyt_cells_count(plyt) ;
	const rect2fp_t* cells = plyt->cells.items.ptr ;
	const int black = 0 ; 
	const int gray = 1 ; 
	int* buffer = calloc(plyt->size[H] * plyt->size[V], sizeof(int)) ;
	// Render cells.
	for (int i = 0; i < n; ++ i) {
		const rect2u_t s = vec_get(&plyt->cells_sizes, i, rect2u_t) ;
		const pixels_t ox = s.pos[X] ;
		const pixels_t oy = s.pos[Y] ;
		const pixels_t sx = s.size[X] ;
		const pixels_t sy = s.size[Y] ;
		render_rect(plyt, buffer,
			ox - plyt->intra_cell_margin,
			oy - plyt->intra_cell_margin,
			sx + 2 * plyt->intra_cell_margin,
			sy + 2 * plyt->intra_cell_margin,
			gray
		) ;
		render_rect(plyt, buffer, ox, oy, sx, sy, 2 + i) ;
	}
	// Render junctions.
	for (int i = 0; i < plyt->junctions[V].items.len; ++ i) {
		junction_t j = vec_get(&plyt->junctions[V], i, junction_t) ;
		if (j.a == OUTER_EDGE || j.b == OUTER_EDGE)
			continue ;
		const pixels_t p = abs_from_rel(cells[j.a].pos[H] + cells[j.a].size[H], plyt->size[H]) ;
		const int ox = int_max(p-5, 0) ;
		const int sx = int_min(10, plyt->size[H] - ox) ;
		const int oy = int_max(abs_from_rel(j.p, plyt->size[V]) + 30, 0) ;
		const int sy = int_min(abs_from_rel(j.s, plyt->size[V]) - 60, plyt->size[V] - oy) ;
		render_rect(plyt, buffer, ox, oy, sx, sy, 2 + plyt->cells_sizes.items.len) ;
	}
	for (int i = 0; i < plyt->junctions[H].items.len; ++ i) {
		junction_t j = vec_get(&plyt->junctions[H], i, junction_t) ;
		if (j.a == OUTER_EDGE || j.b == OUTER_EDGE)
			continue ;
		const pixels_t p = abs_from_rel(cells[j.a].pos[V] + cells[j.a].size[V], plyt->size[V]) ;
		const int ox = int_max(abs_from_rel(j.p, plyt->size[H]) + 30, 0) ;
		const int sx = int_min(abs_from_rel(j.s, plyt->size[H]) - 60, plyt->size[H] - ox) ;
		const int oy = int_max(p - 5, 0) ;
		const int sy = int_min(10, plyt->size[V] - oy) ;
		render_rect(plyt, buffer, ox, oy, sx, sy, 2 + plyt->cells_sizes.items.len) ;
	}
	// Render to file.
	FILE* file = fopen(filename, "w") ;
	fprintf(file, "P2\n%u %u\n%lu\n", plyt->size[H], plyt->size[V], 3+n) ;
	for (int i = 0; i < plyt->size[V]; ++ i) {
		for (int j = 0; j < plyt->size[H]; ++ j) {
			fprintf(file, "%d ", buffer[i*(plyt->size[H])+j]) ;
		}
		fprintf(file, "\n") ;
	}
	free(buffer) ;
	fclose(file) ;
}

bool test_partition() {
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 500) ;
	plyt_split(plyt, H, 0, 500) ;
	plyt_recompute_sizes(plyt) ; // #fixme #doc required ?
	plyt_recompute_junctions(plyt) ;
	plyt_prepare_move(plyt, H, 1) ;
	plyt_control_move_and_suggest(plyt, H, 250) ;
	plyt_move(plyt, H, 250) ;
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	pretty_print(plyt, stderr) ;
	render(plyt, "partition.pgm") ;
	plyt_deinit(plyt) ;
	return true ;
}

bool test_moving_inner_junctions() {
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 500) ;
	plyt_split(plyt, H, 0, 500) ;
	plyt_recompute_sizes(plyt) ; // #fixme #doc required ?
	plyt_recompute_junctions(plyt) ;
	if (plyt_prepare_move(plyt, H, 1)) {
		const pixels_t ctrl = plyt_control_move_and_suggest(plyt, H, 300) ;
		if (ctrl != 300) {
			fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
			return false ;
		}
		plyt_move(plyt, H, ctrl) ;
	}
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	pretty_print(plyt, stderr) ;
	render(plyt, "inner.pgm") ;
	plyt_deinit(plyt) ;
	return true ;
}

bool test_moving_outer_junctions() {
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 500) ;
	if (plyt_prepare_move(plyt, H, 0)) {
		const pixels_t ctrl = plyt_control_move_and_suggest(plyt, H, 300) ;
		if (ctrl != 300) {
			fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
			return false ;
		}
		plyt_move(plyt, H, ctrl) ;
	}
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	if (plyt_prepare_move(plyt, V, 0)) {
		const pixels_t ctrl = plyt_control_move_and_suggest(plyt, V, 100) ;
		if (ctrl != 100) {
			fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
			return false ;
		}
		plyt_move(plyt, V, ctrl) ;
	}
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	pretty_print(plyt, stderr) ;
	render(plyt, "outer.pgm") ;
	plyt_deinit(plyt) ;
	return true ;
}

bool test_moving_and_removing_outer_junctions() {
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 500) ;
	if (plyt_prepare_move(plyt, H, 0)) {
		const pixels_t ctrl = plyt_control_move_and_suggest(plyt, H, 300) ;
		if (ctrl != 300) {
			fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
			return false ;
		}
		plyt_move(plyt, H, ctrl) ;
	}
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	if (plyt_prepare_move(plyt, H, 1)) {
		// const pixels_t ctrl = plyt_control_move_and_suggest(plyt, H, 0) ;
		// if (ctrl != 0) {
		// 	fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
		// 	return false ;
		// }
		// plyt_move(plyt, H, ctrl) ;
		plyt_move(plyt, H, 0) ; // #fixme
	}
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	pretty_print(plyt, stderr) ;
	render(plyt, "outer,remove.pgm") ;
	plyt_deinit(plyt) ;
	return true ;
}

bool test_strange_topology() {
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 500) ;
	// if (plyt_prepare_move(plyt, H, 0)) {
	// 	const pixels_t ctrl = plyt_control_move_and_suggest(plyt, H, 300) ;
	// 	if (ctrl != 300) {
	// 		fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
	// 		return false ;
	// 	}
	// 	plyt_move(plyt, H, ctrl) ;
	// }
	// plyt_recompute_sizes(plyt) ;
	// plyt_recompute_junctions(plyt) ;
	// if (plyt_prepare_move(plyt, H, 1)) {
	// 	// const pixels_t ctrl = plyt_control_move_and_suggest(plyt, H, 0) ;
	// 	// if (ctrl != 0) {
	// 	// 	fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
	// 	// 	return false ;
	// 	// }
	// 	// plyt_move(plyt, H, ctrl) ;
	// 	plyt_move(plyt, H, 0) ; // #fixme
	// }
	// plyt_recompute_sizes(plyt) ;
	// plyt_recompute_junctions(plyt) ;

	// YOU WERE DOING : AN EXEMPLE WITH STRANGE TOPOLOGY

	pretty_print(plyt, stderr) ;
	render(plyt, "outer,remove.pgm") ;
	plyt_deinit(plyt) ;
	return true ;
}

typedef struct {
	pixels_t* mins ;
	pixels_t* defs ;
} arrays_t ;

void from_arrays(int i, pixels_t* mins, pixels_t* defs, void* data) {
	arrays_t* arrays = (arrays_t*) data ;
	*mins = arrays->mins[i] ;
	*defs = arrays->defs[i] ;
}

pixels_t maxp(pixels_t a, pixels_t b, void* useless) {
	return max(a, b) ;
}

bool test_mins_defs_trivial() {
	pixels_t cells_mins[] = {118} ;
	pixels_t cells_defs[] = {218} ;
	arrays_t arrays = { .mins = cells_mins, .defs = cells_defs } ;
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 1000) ;
	pixels_t mins, defs ;
	plyt_min_and_default_sizes(
		plyt, H, &mins, &defs,
		from_arrays, &arrays,
		maxp, NULL
	) ;
	plyt_deinit(plyt) ;
	return (mins == 118) && (defs == 218) ;
}

bool test_mins_defs_complex() {
	pixels_t cells_mins[] = {10, 10, 10, 10, 10, 10} ;
	pixels_t cells_defs[] = {25, 25, 25, 25, 25, 25} ;
	arrays_t arrays = { .mins = cells_mins, .defs = cells_defs } ;
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 1000) ;
	plyt_split(plyt, V, 0, 300) ;
	plyt_split(plyt, H, 0, 300) ;
	plyt_split(plyt, H, 1, 450) ;
	plyt_split(plyt, V, 3, 750) ;
	plyt_split(plyt, H, 4, 750) ;
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	pixels_t mins, defs ;
	plyt_min_and_default_sizes(
		plyt, H, &mins, &defs,
		from_arrays, &arrays,
		maxp, NULL
	) ;
	// render(plyt, "test.pgm") ;
	plyt_deinit(plyt) ;
	return (mins == 30) && (defs == 75) ;
}

bool test_moving_inner_junctions_complex() {
	pixels_t cells_mins[] = {10, 10, 10, 10, 10, 10} ;
	pixels_t cells_defs[] = {25, 25, 25, 25, 25, 25} ;
	arrays_t arrays = { .mins = cells_mins, .defs = cells_defs } ;
	plyt_t* plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 1000) ;
	plyt_split(plyt, V, 0, 300) ;
	plyt_split(plyt, H, 0, 300) ;
	plyt_split(plyt, H, 1, 450) ;
	plyt_split(plyt, V, 3, 750) ;
	plyt_split(plyt, H, 4, 750) ;
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	render(plyt, "inner,complex,before.pgm") ;
	if (plyt_prepare_move(plyt, V, 1)) {
		const pixels_t ctrl = plyt_control_move_and_suggest(plyt, V, 500) ;
		if (ctrl != 500) {
			fprintf(stderr, "Move control (%d) reported an error.\n", ctrl) ;
			return false ;
		}
		plyt_move(plyt, V, ctrl) ;
	}
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	render(plyt, "inner,complex,after.pgm") ;
	plyt_deinit(plyt) ;
	return true ;
}

int main() {
	// eval("vec", test_vec()) ;
	// eval("vec resize", test_vec_resize()) ;
	// eval("vec sort", test_vec_sort()) ;
	// eval("partition", test_partition()) ;
	eval("move complex inner junctions", test_moving_inner_junctions_complex()) ;
	// eval("move inner junctions", test_moving_inner_junctions()) ;
	// eval("move outer junctions", test_moving_outer_junctions()) ;
	eval("move and remove outer junctions", test_moving_and_removing_outer_junctions()) ;
	// eval("mins and defs, single cell", test_mins_defs_trivial()) ;
	// eval("mins and defs, many cells", test_mins_defs_complex()) ;
}
