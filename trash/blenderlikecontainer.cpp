#pragma once

#ifdef BLC_LAYOUT_TESTS
#include <fstream>
#include <iostream>
#endif

#include <iostream> // FIXME remove

#include <cmath>
#include <array>
#include <vector>
#include <memory>
#include <algorithm>

// This file is meant to be included directly into another C++ file (for
// the inlining to work if desired). It provides a data structure which holds
// the state of a layout that behaves like the blender panes layout. It relies
// on fixed point precision to ensure coherent truncations into pixels.

namespace blc {

using ID = int ;
using Pixels = int ;
using FixedPoint = int ;

constexpr ID OUTER_EDGE = 1<<30 ;
constexpr FixedPoint FP_SCALING = 1<<16 ;

enum Axis { H = 0, V = 1 } ;
enum XYZ  { X = 0, Y = 1, Z = 2 } ;

//enum Quadrant { NW = 0, NE = 1, SW = 2, SE = 3 } ;

struct Rect2fp { FixedPoint pos[2] ; FixedPoint size[2] ; } ;
struct Rect2u { Pixels pos[2] ; Pixels size[2] ; } ;

struct Junction { ID a ; ID b ; FixedPoint p ; FixedPoint s ; } ;
struct Interval { FixedPoint a ; FixedPoint b ; ID c ; } ;

int compare_intervals(const Interval& a, const Interval& b) {
	return (a.a < b.a) or ((a.a == b.a) and (a.b < b.b)) ;
}

FixedPoint from_unit_float(float f) {
	return static_cast<FixedPoint>(std::floor(f * FP_SCALING)) ;
}

Pixels abs_from_rel(FixedPoint p, Pixels total) {
	return (p * total) / FP_SCALING ;
}

static void show_fp(const FixedPoint a) {
	std::cerr << a << " (" << ((a != 0) ? (static_cast<double>(a) / FP_SCALING) : 0) << ")" ;
}

static Axis opposite(Axis axis) { return (axis == H) ? V : H ; }

/// When a resizing occurs, relevant junctions that must be modified are
/// retrieved and stored in this structure, alongside the interval in which
/// the current set of junctions can be moved. I'm afraid the code looks like
/// so many other mesh processing algorithms, with few letters variables :>
struct MoveContext {
	std::vector<ID> ca ;
	std::vector<ID> cb ;
	std::vector<Pixels> suggestions ;
	Pixels lim_a = 0 ;
	Pixels lim_b = 0 ;
	int au, av ;
	int bu, bv ;
} ;

/// Attributes of a cell, telling whether they expand, fill, etc...
struct SizeConstraints {
	int expand = 0 ;
	int fill = 0 ;
} ;

/// The main data structure, holding vertical, horizontal, and corner
/// junctions. It also contains a field with cells sizes : be careful,
/// they must be explicitely recomputed from the junctions.
struct BlenderLikeContainer {
	// Cells sizes.
	std::vector<Rect2fp> cells ;
	std::vector<Rect2u> cells_sizes ;
	// Constraints.
	std::vector<SizeConstraints> cells_constraints ;
	int inter_cell_gap = 1 ;
	int intra_cell_margin = 2 ;
	uint size[2] ;
	// Junctions (dependant data).
	std::vector<Junction> junctions[2] ;
	// And some useful other informations.
	//uint lines_junctions_thickness = 10 ; // In pixels.
	MoveContext move_ctx ;

	BlenderLikeContainer(size_t w, size_t h) {
		// const FixedPoint half = from_unit_float(0.5) ;
		const FixedPoint one  = from_unit_float(1.0) ;
		cells.emplace_back(Rect2fp {{0, 0}, {one, one}}) ;
		cells_sizes.emplace_back() ;
		cells_constraints.emplace_back() ;
		resize(w, h) ;
	}

	void add_junction(Axis axis, ID a, ID b, FixedPoint p, FixedPoint s) {
		junctions[axis].emplace_back(Junction {a, b, p, s}) ;
	}

	/// Find all junctions between pairs of cells, in a straightforward
	/// and totally O(n^2) way. The next function might be better on a
	/// large number of cells, but it is still WIP.
	void recompute_junctions() {
		junctions[H].clear() ;
		junctions[V].clear() ;
		for (int i = 0; i < cells.size(); ++ i) {
			const auto iax = cells[i].pos[H] ;
			const auto iay = cells[i].pos[V] ;
			const auto ibx = cells[i].size[H] + iax ;
			const auto iby = cells[i].size[V] + iay ;
			// If one needs outer edges.
			if (0) {
				if (iax == 0)
					add_junction(V, OUTER_EDGE, i, iay, cells[i].size[V]) ;
				if (ibx == FP_SCALING)
					add_junction(V, i, OUTER_EDGE, iay, cells[i].size[V]) ;
				if (iay == 0)
					add_junction(H, OUTER_EDGE, i, iax, cells[i].size[H]) ;
				if (iby == FP_SCALING)
					add_junction(H, i, OUTER_EDGE, iax, cells[i].size[H]) ;
			}
			// Inter-cell junctions.
			for (int j = i+1; j < cells.size(); ++ j) {
				const auto jax = cells[j].pos[H] ;
				const auto jay = cells[j].pos[V] ;
				const auto jbx = cells[j].size[H] + jax ;
				const auto jby = cells[j].size[V] + jay ;
				{
					const auto maxax = std::max(iax, jax) ;
					const auto maxay = std::max(iay, jay) ;
					const auto minbx = std::min(ibx, jbx) ;
					const auto minby = std::min(iby, jby) ;
					if ((maxax == minbx) and (maxay < minby)) {
						const auto ca = (iax < jax) ? i : j ;
						const auto cb = i + j - ca ;
						add_junction(V, ca, cb, maxay, minby - maxay) ;
					} else if ((maxay == minby) and (maxax < minbx)) {
						const auto ca = (iay < jay) ? i : j ;
						const auto cb = i + j - ca ;
						add_junction(H, ca, cb, maxax, minbx - maxax) ;
					}
				}
			}
		}
	}

	/// This is the only complex part of the code. It retrieves junctions
	/// between cells without any prerequesite. It relies on two passes (
	/// one per axis) and sorts intervals to avoid comparing every pairs
	/// of cells. It could still be improved, I think. This is the price
	/// to pay for having such simple code in every other parts.
	void recompute_junctions_better_in_theory_maybe() {
		std::cerr << "............... RECOMPUTE JUNCTIONS" << std::endl ;
		std::vector<Interval> ivals ;
		ivals.reserve(cells.size()) ;
		// Compute junctions on each axis.
		const auto junctions_on_axis = [&ivals, this](Axis axis) {
			const auto oppo = opposite(axis) ;
			junctions[oppo].clear() ;
			// Compute intervals of the cells on the current axis.
			for (int i = 0 ; i < cells.size() ; ++ i) {
				const auto a = cells[i].pos[axis] ;
				const auto b = cells[i].pos[axis] + cells[i].size[axis] ;
				ivals.emplace_back(Interval {a, b, i}) ;
			}
			// Sort the intervals. This avoid being O(n^2) :D.
			std::sort(ivals.begin(), ivals.end(), compare_intervals) ;
			int u = 0 ;
			// Introduce junctions as required.
			for (int i = 0; i < ivals.size(); ++ i) {
				if (ivals[i].a == 0) {
					const auto c = ivals[i].c ;
					//add_junction(oppo, OUTER_EDGE, c, 0, cells[c].size[oppo]) ;
					add_junction(oppo, OUTER_EDGE, c, cells[c].pos[oppo], cells[c].size[oppo]) ;
					std::cerr << "Add side-a junction" << std::endl ;
				}
				if (ivals[i].b == FP_SCALING) {
					const auto c = ivals[i].c ;
					//add_junction(oppo, c, OUTER_EDGE, FP_SCALING, cells[c].size[oppo]) ;
					add_junction(oppo, c, OUTER_EDGE, cells[c].pos[oppo], cells[c].size[oppo]) ;
					std::cerr << "Add side-b junction" << std::endl ;
				}
				//while ((u < ivals.size()) && (ivals[u].a < ivals[i].b)) {
				//	++ u ;
				//}
				u = i + 1 ; // TODO There may be a better way.
				for (int j = u; j < ivals.size(); ++ j) {
					// Because of the sort we can stop early.
					if (ivals[i].b < ivals[j].a) {
						break ;
					}
					const auto ci = ivals[i].c ;
					const auto cj = ivals[j].c ;
					const auto ia = cells[ci].pos[oppo] ;
					const auto ib = cells[ci].pos[oppo] + cells[ci].size[oppo] ;
					const auto ja = cells[cj].pos[oppo] ;
					const auto jb = cells[cj].pos[oppo] + cells[cj].size[oppo] ;
					const auto max_a_ij = std::max(ia, ja) ;
					const auto min_b_ij = std::min(ib, jb) ;
					if (max_a_ij < min_b_ij) {
						add_junction(oppo, ci, cj, max_a_ij, min_b_ij - max_a_ij) ;
						std::cerr << "Add junction" << std::endl ;
					}
				}
			}
			ivals.clear() ;
		} ;
		std::cerr << "[blc][recompute] horizontal" << std::endl ;
		junctions_on_axis(H) ;
		std::cerr << "[blc][recompute] vertical" << std::endl ;
		junctions_on_axis(V) ;
	}

	/// Recompute current absolute size from relative size for cell C.
	void recompute_size(ID c) {
		const auto offset = intra_cell_margin + inter_cell_gap ;
		cells_sizes[c].pos[X] = abs_from_rel(cells[c].pos[X], size[H]) + offset ;
		cells_sizes[c].pos[Y] = abs_from_rel(cells[c].pos[Y], size[V]) + offset ;
		cells_sizes[c].size[X] = abs_from_rel(cells[c].size[X], size[H]) - 2 * offset ;
		cells_sizes[c].size[Y] = abs_from_rel(cells[c].size[Y], size[V]) - 2 * offset ;
	}

	/// Recompute current absolute sizes from relative sizes.
	void recompute_sizes() {
		const auto offset = intra_cell_margin + inter_cell_gap ;
		for (int c = 0; c < cells.size(); ++ c) {
			cells_sizes[c].pos[X] = abs_from_rel(cells[c].pos[X], size[H]) + offset ;
			cells_sizes[c].pos[Y] = abs_from_rel(cells[c].pos[Y], size[V]) + offset ;
			cells_sizes[c].size[X] = abs_from_rel(cells[c].size[X], size[H]) - 2 * offset ;
			cells_sizes[c].size[Y] = abs_from_rel(cells[c].size[Y], size[V]) - 2 * offset ;
		}
	}

	/// Resize the whole container, triggering a resize of the cells, that
	/// tries to honour the constraints of everyone.
	void resize(size_t w, size_t h) {
		size[H] = w ;
		size[V] = h ;
		// Affect cells *relative* sizes to solve constraints.
		// TODO
		// Recompute the *absolute* sizes, and *relative* junctions.
		recompute_sizes() ;
		recompute_junctions() ;
	}

	/// Split a cell vertically (create an horizontal junction). The place
	/// where the cell must be split is specified in pixels from the origin
	/// of the *container* (not the cell).
	void split(Axis axis, ID c, Pixels p) {
		const FixedPoint p_ratio = (p * FP_SCALING) / size[axis] ;
		const auto s = p_ratio - cells[c].pos[axis] ;
		const auto t = cells[c].size[axis] - s ;
		Rect2fp nc = cells[c] ;
		nc.pos[axis] += s ;
		nc.size[axis] = t ;
		cells.emplace_back(nc) ;
		cells_sizes.emplace_back() ;
		cells_constraints.emplace_back() ;
		cells[c].size[axis] = s ;
		//recompute_size(c) ;
		//recompute_size(cells.size()-1) ;
		//recompute_junctions() ;
	}

	/// ...
	void merge(Axis axis, ID j) {
		const auto oppo = opposite(axis) ;
		const auto ca = junctions[oppo][j].a ;
		const auto cb = junctions[oppo][j].b ;
		// // Check if the merge is possible.
		// const auto bad_pos = (cells[ca].pos[oppo] != cells[cb].pos[oppo]) ;
		// const auto bad_size = (cells[ca].size[oppo] != cells[cb].size[oppo]) ;
		// if (bad_pos or bad_size)
		// 	return ;
		// Resize the 'a' cell.
		cells[ca].size[axis] += cells[cb].size[axis] ;
		// Remove the 'b' cell.
		cells.erase(cells.begin() + cb) ;
		cells_sizes.erase(cells_sizes.begin() + cb) ;
		cells_constraints.erase(cells_constraints.begin() + cb) ;
		//recompute_size(ca) ;
		//recompute_junctions() ;
	}

	/// This function is long to read but is actually quite simple, and not
	/// so costly. Anyway, it is ran just once when a user starts moving an
	/// edge.
	bool prepare_move(Axis axis, ID junction) {
		const Axis oppo = opposite(axis) ;
		Junction* j = &junctions[oppo][junction] ;
		// std::cerr << "- Junction asked joins (" << j->a << ", " << j->b << ")" << std::endl ;
		if ((j->a == OUTER_EDGE) or (j->b == OUTER_EDGE))
			return false ;
		// This part walks over the implicit edge formed by a possible seq.
		// of aligned junctions, and list every junction. For this it takes
		// the relative position of the junction and list every cells that
		// have one boundary at such position *and* is connect directly or
		// indirectly to j->
		const FixedPoint p = cells[j->b].pos[axis] ;
		//std::cerr << "- Trying to move edge at position " ; show_fp(p) ; std::cerr << std::endl ;
		move_ctx.ca.clear() ;
		move_ctx.cb.clear() ;
		move_ctx.ca.reserve(cells.size()) ;
		move_ctx.cb.reserve(cells.size()) ;
		for (int i = 0; i < cells.size(); ++ i) {
			const auto a = cells[i].pos[axis] ;
			const auto b = cells[i].pos[axis] + cells[i].size[axis] ;
			if (a == p)	move_ctx.cb.push_back(i) ;
			if (b == p)	move_ctx.ca.push_back(i) ;
		}
		// Intervals on each side of the edge are sorted along the edge.
		// This makes it "easy" to find the region we're looking for.
		const auto sort_cells =	[this, oppo](const ID a, const ID b) -> int	{
			return cells[a].pos[oppo] < cells[b].pos[oppo] ;
		} ;
		std::sort(move_ctx.ca.begin(), move_ctx.ca.end(), sort_cells) ;
		std::sort(move_ctx.cb.begin(), move_ctx.cb.end(), sort_cells) ;
		// Look for j->a while maintaining continuity. Index u represents
		// the first interval (or cell) in ivals_a that is know to be
		// "reachable" from j.
		const int lim = move_ctx.ca.size() ;
		int i = 0, u = 0 ;
		while ((i < lim - 1) and (move_ctx.ca[i] != j->a)) {
			const auto curr = &cells[move_ctx.ca[i]] ;
			const auto next = &cells[move_ctx.ca[i+1]] ;
			const auto curr_end = curr->pos[oppo] + curr->size[oppo] ;
			const auto next_begin = next->pos[oppo] ;
			i += 1 ;
			if (curr_end < next_begin)
				u = i ;
		}
		// Now i is necessarily on the correct interval. So we can look for
		// the end of the chain with a second index v.
		int v = i ;
		while (i < lim - 1) {
			const auto curr = &cells[move_ctx.ca[i]] ;
			const auto next = &cells[move_ctx.ca[i+1]] ;
			const auto curr_end = curr->pos[oppo] + curr->size[oppo] ;
			const auto next_begin = next->pos[oppo] ;
			if (curr_end < next_begin) {
				v = i ;
				break ;
			}
			++ i ;
		}
		if (i == lim - 1) {
			v = i ;
		}
		move_ctx.au = u ;
		move_ctx.av = v ;
		const FixedPoint bound_u = cells[move_ctx.ca[move_ctx.au]].pos[oppo] ;
		const FixedPoint bound_v = cells[move_ctx.ca[move_ctx.av]].pos[oppo] +
								   cells[move_ctx.ca[move_ctx.av]].size[oppo] ;
		// std::cerr << "- Bound u: " ; show_fp(bound_u) ; std::cerr << std::endl ;
		// std::cerr << "- Bound v: " ; show_fp(bound_v) ; std::cerr << std::endl ;
		// Now the correspondance in ivals_b can be found easily.
		for (i = 0; i < move_ctx.cb.size(); ++ i) {
			const auto ci = &cells[move_ctx.cb[i]] ;
			if (ci->pos[oppo] == bound_u) u = i ;
			if (ci->pos[oppo] + ci->size[oppo] == bound_v) v = i ;
		}
		move_ctx.bu = u ;
		move_ctx.bv = v ;
		if (0) {
			// Now, if needed, we can trim the selection to stop at junctions with
			// valence 4. This is a matter of intuition for the user.
			// TODO
		}
		// std::cerr << "- Indices au: " << move_ctx.au << ", av: " << move_ctx.av << std::endl ;
		// std::cerr << "- Indices bu: " << move_ctx.bu << ", bv: " << move_ctx.bv << std::endl ;
		move_ctx.lim_a = 0 ;
		move_ctx.lim_b = size[axis] ;
		// Compute the interval in which the full edge can be moved.
		for (i = move_ctx.au; i <= move_ctx.av; ++ i) {
			const auto cell_lim = cells_sizes[move_ctx.ca[i]].pos[axis] ;
			move_ctx.lim_a = std::max(move_ctx.lim_a, cell_lim) ;
		}
		for (i = move_ctx.bu; i <= move_ctx.bv; ++ i) {
			const auto cell_lim = cells_sizes[move_ctx.cb[i]].pos[axis] +
								  cells_sizes[move_ctx.cb[i]].size[axis] ;
			move_ctx.lim_b = std::min(move_ctx.lim_b, cell_lim) ;
		}
		move_ctx.lim_a += (inter_cell_gap + intra_cell_margin + 1) ;
		move_ctx.lim_b -= (inter_cell_gap + intra_cell_margin + 1) ;
		if (move_ctx.lim_a >= move_ctx.lim_b)
			return false ;
		// std::cerr << "- Lim. a: " << move_ctx.lim_a << std::endl ;
		// std::cerr << "- Lim. b: " << move_ctx.lim_b << std::endl ;
		// Find adjacent cells to propose. 'eu' and 'ev' are *edge* [u, v].
		move_ctx.suggestions.clear() ;
		const auto eu = cells[move_ctx.ca[move_ctx.au]].pos[oppo] ;
		const auto ev = cells[move_ctx.ca[move_ctx.av]].pos[oppo] +
					    cells[move_ctx.ca[move_ctx.av]].size[oppo] ;
		// std::cerr << "- Edge u: " ; show_fp(eu) ; std::cerr << std::endl ;
		// std::cerr << "- Edge v: " ; show_fp(ev) ; std::cerr << std::endl ;
		for (int i = 0; i < cells.size(); ++ i) {
			if (cells[i].pos[axis] != 0) {
				const auto cu = cells[i].pos[oppo] ;
				const auto cv = cells[i].pos[oppo] + cells[i].size[oppo] ;
				//std::cerr << "- Cell u: " ; show_fp(cu) ; std::cerr << std::endl ;
				//std::cerr << "- Cell v: " ; show_fp(cv) ; std::cerr << std::endl ;
				if ((cu == ev) or (cv == eu)) {
					//std::cerr << "Suggest !" << std::endl ;
					move_ctx.suggestions.push_back(cells_sizes[i].pos[axis] - intra_cell_margin - inter_cell_gap) ;
				}
			}
		}
		// std::cerr << "Suggestions:" << std::endl ;
		// for (const auto suggestion: move_ctx.suggestions) {
		// 	std::cerr << "- " << suggestion << std::endl ;
		// }
		return true ;
	}

	Pixels control_move_and_suggest(Axis axis, Pixels p) {
		const auto oppo = opposite(axis) ;
		const auto clamped = std::max(move_ctx.lim_a, std::min(p, move_ctx.lim_b)) ;
		if (clamped != p)
			return clamped ;
		int best_sug ;
		int min_dist = size[axis] ;
		for (int i = 0; i < move_ctx.suggestions.size(); ++ i) {
			const auto sug = move_ctx.suggestions[i] ;
			if ((sug < move_ctx.lim_a) or (sug > move_ctx.lim_b))
				continue ;
			const auto dist = abs(sug - clamped) ;
			if (min_dist > dist) {
				min_dist = dist ;
				best_sug = sug ;
			}
		}
		return (min_dist < 10) ? best_sug : clamped ;
	}

	void move(Axis axis, Pixels p) {
		const FixedPoint ratio = (p * FP_SCALING) / size[axis] ;
		for (int i = move_ctx.au; i <= move_ctx.av; ++ i) {
			const auto ca = move_ctx.ca[i] ;
			cells[ca].size[axis] = ratio - cells[ca].pos[axis] ;
		}
		for (int i = move_ctx.bu; i <= move_ctx.bv; ++ i) {
			const auto cb = move_ctx.cb[i] ;
			cells[cb].size[axis] = cells[cb].pos[axis] + cells[cb].size[axis] - ratio ;
			cells[cb].pos[axis] = ratio ;
		}
		//recompute_sizes() ;
		//recompute_junctions() ;
	}

	void show() {
#ifdef BLC_RENDER_TESTS
		std::cerr << "Cells:" << std::endl ;
		// for (const auto& cell: cells_sizes) {
		// 	std::cerr << "- " << cell.pos[X] << " " << cell.pos[Y] << " "
		// 		<< cell.size[X] << " " << cell.size[Y] << std::endl ;
		// }
		for (const auto& cell: cells) {
			std::cerr << "- " ;
			show_fp(cell.pos[X]) ; std::cerr << " " ;
			show_fp(cell.pos[Y]) ; std::cerr << " " ;
			show_fp(cell.size[X]) ; std::cerr << " " ;
			show_fp(cell.size[Y]) ; std::cerr << std::endl ;
		}
		for (int axis = 0; axis < 2; ++ axis) {
			std::cerr << "Junctions (axis #" << axis << "): " << std::endl ;
			for (const auto& j: junctions[(Axis) axis]) {
				std::cerr << "- (" << j.a << ", " << j.b << "): " ;
				show_fp(j.p) ; std::cerr << ", " ;
				show_fp(j.s) ; std::cerr << std::endl ;
			}
		}
#endif
	}

	void render_rect(int* buffer, int ox, int oy, int sx, int sy, int col) {
#ifdef BLC_RENDER_TESTS
		for (int i = 0; i < sy; ++ i)
			for (int j = 0; j < sx; ++ j)
				buffer[(oy+i)*size[H]+(ox+j)] = col ;
#endif
	}

	void render(const std::string& fname) { // to .pgm
#ifdef BLC_RENDER_TESTS
		const int black = 0 ; 
		const int gray = 1 ; 
		std::vector<int> buffer(size[H] * size[V], black) ;
		// Render cells.
		for (int i = 0; i < cells_sizes.size(); ++ i) {
			const auto ox = cells_sizes[i].pos[X] ;
			const auto oy = cells_sizes[i].pos[Y] ;
			const auto sx = cells_sizes[i].size[X] ;
			const auto sy = cells_sizes[i].size[Y] ;
			render_rect(buffer.data(),
				ox - intra_cell_margin,
				oy - intra_cell_margin,
				sx + 2 * intra_cell_margin,
				sy + 2 * intra_cell_margin,
				gray
			) ;
			render_rect(buffer.data(), ox, oy, sx, sy, 2 + i) ;
		}
		// Render junctions.
		for (const auto& j: junctions[V]) {
			if (j.a == OUTER_EDGE or j.b == OUTER_EDGE)
				continue ;
			const auto p = abs_from_rel(cells[j.a].pos[H] + cells[j.a].size[H] , size[H]) ;
			render_rect(buffer.data(),
				p - 5, abs_from_rel(j.p, size[V]) + 30,
				10,    abs_from_rel(j.s, size[V]) - 60,
				2 + cells_sizes.size()
			) ;
		}
		for (const auto& j: junctions[H]) {
			if (j.a == OUTER_EDGE or j.b == OUTER_EDGE)
				continue ;
			const auto p = abs_from_rel(cells[j.a].pos[V] + cells[j.a].size[V] , size[V]) ;
			render_rect(buffer.data(),
				abs_from_rel(j.p, size[H]) + 30, p - 5,
				abs_from_rel(j.s, size[H]) - 60, 10,
				2 + cells_sizes.size()
			) ;
		}
		// Render to file.
		auto os = std::ofstream(fname) ;
		os << "P2" << std::endl ;
		os << size[H] << " " << size[V] << std::endl ;
		os << 3 + cells_sizes.size() << std::endl ;
		for (int i = 0; i < size[V]; ++ i) {
			for (int j = 0; j < size[H]; ++ j) {
				os << buffer[i*size[H]+j] << " " ;
			}
			os << std::endl ;
		}
#endif
	}
} ;


// ..................................................................... TESTS
#ifdef BLC_LAYOUT_TESTS
#define BLC_RUN_OR_ERR(F) \
	if (F) { std::cerr << "- "#F" suceed." << std::endl ; } \
	else { std::cerr << "- "#F" failed (line " << __LINE__ << ")." << std::endl ; }

#define BLC_EQUAL_OR_FAIL(A, B) \
	if ((A) != (B)) { std::cerr << "- equality false (line " \
		<< __LINE__ << ")" << std::endl ; return 0 ; }

int test_precision() {
	BLC_EQUAL_OR_FAIL(abs_from_rel(82, from_unit_float(0.5)), 41) ;
	BLC_EQUAL_OR_FAIL(abs_from_rel(293, from_unit_float(1.0)), 293) ;
	BLC_EQUAL_OR_FAIL(abs_from_rel(1932, from_unit_float(0.0)), 0) ;
	//BLC_EQUAL_OR_FAIL(apply_ratio(4887, from_unit_float(1.0/9.0)), 543) ;
	// This last line is false at 1<<20 but true at 1<<30 in precision.
	return 1 ;
}

int test_init() {
	BlenderLikeContainer container(100, 554) ;
	container.intra_cell_margin = 2 ;
	container.inter_cell_gap = 1 ;
	container.resize(1000, 2000) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].pos[X], 3) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].pos[Y], 3) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].size[X], 994) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].size[Y], 1994) ;
	container.intra_cell_margin = 0 ;
	container.inter_cell_gap = 0 ;
	container.resize(1920, 1080) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].pos[X], 0) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].pos[Y], 0) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].size[X], 1920) ;
	BLC_EQUAL_OR_FAIL(container.cells_sizes[0].size[Y], 1080) ;
	return 1 ;
}

int test_splits() {
	BlenderLikeContainer container(1000, 2000) ;
	container.split(H, 0, 500) ;
	container.split(V, 0, 1500) ;
	container.split(V, 0, 750) ;
	container.split(V, 1, 500) ;
	container.split(H, 1, 750) ;
	container.recompute_junctions() ;
	container.recompute_sizes() ;
	container.show() ;
	container.render("splits.pgm") ;
	return 1 ;
}

int test_merge() {
	BlenderLikeContainer container(1000, 2000) ;
	container.split(H, 0, 500) ;
	container.split(V, 0, 1500) ;
	container.split(V, 0, 750) ;
	container.split(V, 1, 500) ;
	container.split(H, 1, 750) ;
	container.recompute_junctions() ;
	container.merge(H, 2) ;
	container.recompute_junctions() ;
	container.recompute_sizes() ;
	container.show() ;
	container.render("merge.pgm") ;
	return 1 ;
}

int test_move() {
	BlenderLikeContainer container(1920, 1080) ;
	container.split(H, 0, 1200) ;
	container.split(H, 1, 1400) ;
	container.split(V, 0, 400) ;
	container.split(V, 2, 400) ;
	container.split(H, 0, 400) ;
	container.split(H, 5, 652) ;
	container.split(H, 6, 900) ;
	container.split(V, 3, 800) ;
	container.split(H, 3, 760) ;
	container.split(H, 4, 1578) ;
	container.recompute_junctions() ;
	container.recompute_sizes() ;
	container.show() ;
	container.render("move,0.pgm") ;
	if (container.prepare_move(V, 2)) {
		const auto sug = container.control_move_and_suggest(V, 680) ;
		container.move(V, sug) ;
		container.recompute_junctions() ;
		container.recompute_sizes() ;
		container.show() ;
		container.render("move,1.pgm") ;
	} else {
		std::cerr << "Can't move." << std::endl ;
	}
	if (container.prepare_move(V, 7)) {
		const auto sug = container.control_move_and_suggest(V, 620) ;
		container.move(V, sug) ;
		container.recompute_junctions() ;
		container.recompute_sizes() ;
		container.show() ;
		container.render("move,2.pgm") ;
	} else {
		std::cerr << "Can't move." << std::endl ;
	}
	if (container.prepare_move(H, 9)) {
		const auto sug = container.control_move_and_suggest(H, 767) ;
		container.move(H, sug) ;
		container.recompute_junctions() ;
		container.recompute_sizes() ;
		container.show() ;
		container.render("move,3.pgm") ;
	} else {
		std::cerr << "Can't move." << std::endl ;
	}
	if (container.prepare_move(V, 3)) { // 0 doesn't work
		const auto sug = container.control_move_and_suggest(V, 200) ;
		container.move(V, sug) ;
		container.recompute_junctions() ;
		container.recompute_sizes() ;
		container.show() ;
		container.render("move,4.pgm") ;
	} else {
		std::cerr << "Can't move." << std::endl ;
	}
	return 1 ;
}

/// Main function to run tests.
void run_tests() {
	//BLC_RUN_OR_ERR(test_precision()) ;
	BLC_RUN_OR_ERR(test_init()) ;
	BLC_RUN_OR_ERR(test_splits()) ;
	BLC_RUN_OR_ERR(test_merge()) ;
	BLC_RUN_OR_ERR(test_move()) ;
}
#endif
// ...........................................................................

} // end namespace blc
