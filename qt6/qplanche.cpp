#include "qplanche.hpp"
#include "qplanchehandle.hpp"

#include <QWidget>
#include <QPushButton>

#include <iostream>

QPlanche::QPlanche(QWidget* parent)
	: QLayout(parent)
{
	plyt = plyt_init() ;
	plyt_resize(plyt, 1000, 500) ; // #fixme
	plyt_recompute_junctions(plyt) ; // #fixme
}

QPlanche::~QPlanche() {
	plyt_deinit(plyt) ;
	QLayoutItem* item ;
	while ((item = takeAt(0)))
		delete item ;
}

QSize QPlanche::minimumSize() const {
	return QSize(min_size_x, min_size_y) ;
}

QSize QPlanche::maximumSize() const {
	return QSize(def_size_x, def_size_y) ;
}

QSize QPlanche::sizeHint() const {
	return QSize(def_size_x, def_size_y) ;
}

void QPlanche::setGeometry(const QRect& r) {
	QLayout::setGeometry(r) ;
	plyt_resize(plyt, r.width(), r.height()) ;
	const auto n = count() ;
	for (auto i = 0; i < n; ++ i) {
		auto rect = plyt_abs_cell(plyt, i) ;
		auto item = itemAt(i) ;
		// std::cout << "("
		// 	<< rect.pos[0]  << ", " << rect.pos[1]  << ", "
		// 	<< rect.size[0] << ", " << rect.size[1] << ") vs min. size ("
		// 	<< item->minimumSize().width() << ", " << item->minimumSize().height()
		// 	<< ")" << std::endl ;
		item->setGeometry(QRect(
			rect.pos[0],  rect.pos[1],
			rect.size[0], rect.size[1]
		)) ;
		// #todo assert the size constraints are satisfied.
	}
	resizeHandles() ;
}

bool QPlanche::isEmpty() const {
	return items.size() > 0 ;
}

typedef struct {
	pixels_t* mins ;
	pixels_t* defs ;
} arrays_t ;

static
void from_arrays(int i, pixels_t* mins, pixels_t* defs, void* data) {
	arrays_t* arrays = (arrays_t*) data ;
	*mins = arrays->mins[i] ;
	*defs = arrays->defs[i] ;
}

static
pixels_t maxp(pixels_t a, pixels_t b, void*) {
	return std::max(a, b) ;
}

void QPlanche::recomputeMinAndDefSizes() {
	// The first loop is just about getting min. and default sizes of
	// all the children.
	const auto nc = plyt_cells_count(plyt) ;
	std::vector<pixels_t> hmins(nc) ;
	std::vector<pixels_t> hdefs(nc) ;
	std::vector<pixels_t> vmins(nc) ;
	std::vector<pixels_t> vdefs(nc) ;
	for (auto i = 0u; i < nc; ++ i) {
		const auto m = items[i]->minimumSize() ;
		const auto d = items[i]->sizeHint() ;
		hmins[i] = m.width() ;
		hdefs[i] = d.width() ;
		vmins[i] = m.height() ;
		vdefs[i] = d.height() ;
	}
	// Then we compute the container geometry, in both directions.
	arrays_t arrs {hmins.data(), hdefs.data()} ;
    plyt_min_and_default_sizes(
    	plyt, H,
    	&min_size_x,
    	&def_size_x,
    	from_arrays, &arrs,
    	maxp, nullptr
    ) ;
    arrs = arrays_t {vmins.data(), vdefs.data()} ;
    plyt_min_and_default_sizes(
    	plyt, V,
    	&min_size_y,
    	&def_size_y,
    	from_arrays, &arrs,
    	maxp, nullptr
    ) ;
}

void QPlanche::addHandles() {
	addCornerHandles() ;
	addJunctionHandles() ;
}

void QPlanche::clearHandles() {
	clearCornerHandles() ;
	clearJunctionHandles() ;
}

void QPlanche::resizeHandles() {
	resizeCornerHandles() ;
	resizeJunctionHandles() ;
}

void QPlanche::addCornerHandles() {
	const uint nc = plyt_cells_count(plyt) ;
	for (uint i = 0; i < nc; ++ i) {
		QWidget* handle = new QPlancheHandle() ;
		addChildWidget(handle) ;
		addItem(new QWidgetItem(handle)) ; // is it not redundant ?
	}
}

void QPlanche::clearCornerHandles() {
	const uint nc = plyt_cells_count(plyt) ;
	QLayoutItem* item ;
	while ((item = takeAt(nc))) {
		removeWidget(item->widget()) ;
		removeItem(item) ; // #check
		delete item->widget() ;
		delete item ;
		// #fixme understand this better...
	}
}

void QPlanche::resizeCornerHandles() {
	const uint nc = plyt_cells_count(plyt) ;
	const uint hs = 25 ; // #fixme
	for (uint i = 0; i < nc; ++ i) {
		plyt_pixels_rectangle rect = plyt_abs_cell(plyt, i) ;
		QRect geom(
			rect.pos[X] + rect.size[X] - hs,
			rect.pos[Y] + rect.size[Y] - hs,
			hs, hs
		) ;
		items[nc+i]->setGeometry(geom) ;
	}
}

void QPlanche::addJunctionHandles() {
	const uint njh = plyt_junctions_count(plyt, H) ;
	const uint njv = plyt_junctions_count(plyt, V) ;
	const uint nj = njh + njv ;
	for (uint i = 0; i < nj; ++ i) {
		QWidget* handle = new QPlancheHandle() ;
		addChildWidget(handle) ;
		addItem(new QWidgetItem(handle)) ; // is it not redundant ?
	}
}

void QPlanche::clearJunctionHandles() {
	const uint nc  = plyt_cells_count(plyt) ;
	const uint njh = plyt_junctions_count(plyt, H) ;
	const uint njv = plyt_junctions_count(plyt, V) ;
	const uint nj  = njh + njv ;
	QLayoutItem* item ;
	while ((item = takeAt(nc+nj))) {
		removeWidget(item->widget()) ;
		removeItem(item) ; // #check
		delete item->widget() ;
		delete item ;
		// #fixme understand this better...
	}
}

void QPlanche::resizeJunctionHandles() {
	const uint nc  = plyt_cells_count(plyt) ;
	for (uint axis = 0; axis < 2; ++ axis) {
		const axis_t oppo = (axis == H) ? V : H ;
		const uint nj = plyt_junctions_count(plyt, (axis_t)axis) ;
		for (uint i = 0; i < nj; ++ i) {
			plyt_pixels_rectangle rect = plyt_abs_junction(plyt, (axis_t)axis, i) ;
			// Small Qt correction. #fixme #layout
			// Maybe Qt isn't able to give the exact size, ruining the thing.
			// I shall try with GTK...
			rect.pos[oppo] -= 10 ;
			rect.size[oppo] = 25 ;
			QRect geom(rect.pos[X], rect.pos[Y], rect.size[X], rect.size[Y]) ;
			items[nc+nj+i]->setGeometry(geom) ;
		}
	}
}

static
pixels_t meh = 800 ;

void QPlanche::addWidget(QWidget* widget) {
    // A mockup... #fixme #remove
    if (items.size() > 0) {
    	clearHandles() ; // valid.
    	plyt_split(plyt, H, 0, meh) ;
    	meh -= 200 ;
    }
    plyt_recompute_sizes(plyt) ;
    plyt_recompute_junctions(plyt) ;
    // Valid part.
    addChildWidget(widget) ;
    addItem(new QWidgetItem(widget)) ; // is it not redundant ?
    // #todo find a better place.
    recomputeMinAndDefSizes() ;
    addHandles() ;
    invalidate() ;
}

void QPlanche::addItem(QLayoutItem* item) {
	items.append(item) ;
}

int QPlanche::count() const {
	return items.size() ;
}

QLayoutItem* QPlanche::itemAt(int index) const {
	return items.value(index) ;
}

QLayoutItem* QPlanche::takeAt(int index) {
	if ((index >= 0) && (index < items.size()))
		return items.takeAt(index) ;
	return nullptr ;
}
