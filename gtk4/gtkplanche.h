#pragma once

// See:
// https://github.com/GNOME/gtk/blob/main/gtk/gtkpaned.c
// https://github.com/GNOME/gtk/blob/main/gtk/gtkpaned.h

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTK_TYPE_PLANCHE 	(gtk_planche_get_type())
#define GTK_PLANCHE(obj)   	(G_TYPE_CHECK_INSTANCE_CAST((obj), GTK_TYPE_PLANCHE, GtkPlanche))
#define GTK_IS_PLANCHE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GTK_TYPE_PLANCHE))

typedef struct _GtkPlanche GtkPlanche ;

GDK_AVAILABLE_IN_ALL
GType gtk_planche_get_type(void) G_GNUC_CONST ;

GDK_AVAILABLE_IN_ALL
GtkWidget* gtk_planche_new(void) ;

GDK_AVAILABLE_IN_ALL
GtkWidget* gtk_planche_add_child(GtkPlanche* p, GtkWidget* child) ;

// GDK_AVAILABLE_IN_ALL
// void gtk_planche_set_child(GtkPlanche* p, guint i, GtkWidget* child) ;

// GDK_AVAILABLE_IN_ALL
// GtkWidget* gtk_planche_get_child(GtkPlanche* p, guint i) ;

G_DEFINE_AUTOPTR_CLEANUP_FUNC(GtkPlanche, g_object_unref)

G_END_DECLS
