#include <gtk/gtk.h>
#include "gtkplanche.h"

#include "../src/planche.h"
#include "gtkplanchehandleprivate.h"

static void activate(
    GtkApplication* app,
    gpointer user_data
) {
    GtkWindow* window ;
    window = GTK_WINDOW(gtk_application_window_new(app)) ;
    gtk_window_set_title(window, "Window") ;
    gtk_window_set_default_size(window, 1000, 600) ;
// #define PLANCHE_DEBUG_HANDLE
#ifndef PLANCHE_DEBUG_HANDLE
    GtkWidget* btn0 = gtk_button_new_with_label("A") ;
    GtkWidget* btn1 = gtk_button_new_with_label("B") ;
    // GtkWidget* btn2 = gtk_button_new_with_label("C") ;
    GtkWidget* planche = gtk_planche_new() ;
    // #fixme this specific function should be replaced by the
    // common gtk_widget_set_parent thing.
    gtk_planche_add_child(GTK_PLANCHE(planche), GTK_WIDGET(btn0)) ;
    gtk_planche_add_child(GTK_PLANCHE(planche), GTK_WIDGET(btn1)) ;
    // gtk_planche_add_child(GTK_PLANCHE(planche), GTK_WIDGET(btn2)) ;
#else
    GtkWidget* planche = gtk_box_new(GTK_ORIENTATION_VERTICAL, 1) ;
    GtkWidget* handle  = gtk_planche_handle_new() ;
    gtk_widget_set_parent(handle, planche) ;
#endif
    gtk_window_set_child(window, planche) ;
    gtk_window_present(window) ;
}

int main(int argc, char* argv[]) {
    GtkApplication* app ;
    app = gtk_application_new("org.gtk.example", G_APPLICATION_DEFAULT_FLAGS) ;
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL) ;
    int status = g_application_run(G_APPLICATION(app), argc, argv) ;
    g_object_unref(app) ;
    return status;
}
