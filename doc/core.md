# Core algorithms

- All of the layout functionalities are handled in the `src/planche.c` file. This is more convenient for integration.
- A clear distinction exists between **relative** and **absolute** measures. Relative measures are normalized w.r.t the dimension of the container, and are represented in fixed point `fp_t`. Absolute measures are in pixels `pixels_t`.
- In variable names, `a` and `b` almost always refer to resp. left/up and right/bottom. When `a/b` are already taken, `u/v` is used with the same signification.

## Data structure

### Cells

The single source of truth for the layout is a set of relative cells. That is, rectangles with an origin point (in the upper left) and a size, both defined w.r.t the full size of the container.

Junctions, as well as absolute cells, must be recomputed from these relative cells, using `recompute_junctions` and `recompute_sizes`.

### Junctions

Junctions represent the adjacency of two cells, and store this pair of cells ids as well as a position and size **along their axis**.

There are both horizontal and vertical junctions. Here *the terminology might reveal to be confusing, be careful*.

### Drag context `move_context_t`

When a dragging occurs, a few things must be accounted for. This structure contains the following informations:

- A single junction might be dragged.
- Multiple aligned junctions might be dragged.
- The junction(s) dragged are limited in how far they can be dragged, depending on the topology. This is precomputed at the beginning of a dragging operation.

### Drop context `drop_context_t`

Sometimes the user may *drag and drop* a cell to move it. This structure contains the required informations, such as the initial cell, the cell onto which the initial cell was dropped, and othe contextual informations. The code for this is not too dense.

### Other properties

- Inter-cell gap #todo
- Intra-cell margin #todo

## Algorithms

### Splitting

Splitting is straigtforward. One takes a cell, an axis and a position and split it if the position is within the bounds of the cell (on the relevant axis).

### Merging

Merging would be straightforward if it were not for the usual problem of deleting an item in a contiguous sequence. The choice is made to simply delete the cell and invalidate junctions and cell sizes : *they must be recomputed*.

### Preparing a junction move/drag

This is the most tedious part. Essentially it goes along the lines of :

1. List all cells that share a side with the junction position. This filters all cells that should be considered when looking for the cell chains (see later). Cells that are above this line are stored in ca, and those below in cb.
2. Sort ca and cb cells, according to their positions.
3. Now we determine the range of cells that are effectively located on the line to be moved. This is obtained by finding the set of contiguous cells that contains the junction->a in ca and the junction->b in cb. This is why we sorted those vectors. The algorithm is effectively to iterate over cells and keep track of the last cell that was contiguous with the current group of cells (variables i, u and v). This is done on one side of the line and is trivially found for the other side. In practice, this is made much more complicated by the presence of OUTER_EDGE in some cases.
4. Compute the maximum amount of pixels allowed on each side of the line (the minimum of the sizes of all cells in ca and cb).
5. List all pixels that could be used as suggestions. This should also include 0 and 1 (relatife) #todo.
6. Ideally this should also take into account the minimum size of each cells, but this is a #todo.

