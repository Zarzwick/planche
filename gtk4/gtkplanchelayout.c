#include "gtkplanchelayout.h"

#include "../src/planche.h"

struct _GtkPlancheLayout {
    GtkLayoutManager parent_instance;
    plyt_t* layout ;

    uint nchildren ;
    uint njunctions ;
    uint ncorners ;

    // pixels_t     min_size_x ; /// Minimum horizontal size.
    // pixels_t     min_size_y ; /// Minimum vertical size.
    // pixels_t     def_size_x ; /// Default horizontal size.
    // pixels_t     def_size_y ; /// Default vertical size.
} ;

G_DEFINE_TYPE(GtkPlancheLayout, gtk_planche_layout, GTK_TYPE_LAYOUT_MANAGER)

/// Measure the extent of the full planche, based on the measure of
/// each of its children.
static
void gtk_planche_layout_measure(
    GtkLayoutManager* layout_manager,
    GtkWidget* widget,
    GtkOrientation orientation,
    int for_size,
    int* minimum,
    int* natural,
    int* minimum_baseline,
    int* natural_baseline
) {
    // fprintf(stderr, "layout measure\n") ;
    GtkWidget *child;
    for (child = gtk_widget_get_first_child(widget) ; child ;
         child = gtk_widget_get_next_sibling(child))
    {   
        if (gtk_widget_should_layout(child)) {
            int child_min = 0 ;
            int child_nat = 0 ;
            int child_min_baseline = -1 ;
            int child_nat_baseline = -1 ;
            gtk_widget_measure(
                child, orientation, for_size,
                &child_min, &child_nat,
                &child_min_baseline, &child_nat_baseline) ;
            *minimum = MAX(*minimum, child_min) ;
            *natural = MAX(*natural, child_nat) ;
            if (minimum_baseline && (child_min_baseline > -1))
                *minimum_baseline = MAX(*minimum_baseline, child_min_baseline) ;
            if (natural_baseline && (child_nat_baseline > -1))
                *natural_baseline = MAX(*natural_baseline, child_nat_baseline) ;
        }
    }
    // #fixme modify this for plyt.
}

/// Create a GtkAllocation for a plyt rectangle.
static
GtkAllocation gtk_planche_layout_alloc_from_plyt(plyt_pixels_rectangle r) {
    return (GtkAllocation) {
        .x = r.pos[X],
        .y = r.pos[Y],
        .width = r.size[X],
        .height = r.size[Y]
    } ;
}

/// Allocate (assign) a size and position to the children. It also
/// allocates size and positions for the handles.
static
void gtk_planche_layout_allocate(
    GtkLayoutManager* layout_manager,
    GtkWidget* widget,
    int width,
    int height,
    int baseline
) {
    GtkPlancheLayout* self = GTK_PLANCHE_LAYOUT(layout_manager) ;
    plyt_resize(self->layout, width, height) ;
    const uint njh = plyt_junctions_count(self->layout, H) ;
    const uint njv = plyt_junctions_count(self->layout, V) ;
    const uint nj = njh + njv ;
    const uint nc = plyt_cells_count(self->layout) ;
    uint i = 0u ;
    GtkWidget* child ;
    for (child = gtk_widget_get_first_child(widget) ; child && (i < nc) ;
         child = gtk_widget_get_next_sibling(child), ++ i)
    {
        if (child && gtk_widget_should_layout(child)) {
            const plyt_pixels_rectangle r = plyt_abs_cell(self->layout, i) ;
            const GtkAllocation alloc = gtk_planche_layout_alloc_from_plyt(r) ;
            gtk_widget_size_allocate(child, &alloc, baseline) ;
        } else {
            fprintf(stderr, "UNLAYOUTED WIDGET\n") ;
        }
    }
    // Corner handles.
    for (i = 0; child && (i < nc) ;
         child = gtk_widget_get_next_sibling(child), ++ i)
    {
        if (child && gtk_widget_should_layout(child)) {
            const plyt_pixels_rectangle r = plyt_abs_cell(self->layout, i) ;
            GtkAllocation alloc ;
            alloc.x = r.pos[X] + r.size[X] - 25 ;
            alloc.y = r.pos[Y] + r.size[Y] - 25 ;
            alloc.width = 25 ;
            alloc.height = 25 ;
            // fprintf(stderr, "layouting corner %d\n", i) ;
            gtk_widget_size_allocate(child, &alloc, baseline) ;
        } else {
            fprintf(stderr, "UNLAYOUTED CORNER\n") ;
        }
    }
    // (Horizontal) junction handles.
    for (i = 0; child && (i < njh) ;
         child = gtk_widget_get_next_sibling(child), ++ i)
    {
        if (child && gtk_widget_should_layout(child)) {
            // fprintf(stderr, "layouting horizontal handle %d\n", i) ;
            plyt_pixels_rectangle r = plyt_abs_junction(self->layout, H, i) ;
            const GtkAllocation alloc = gtk_planche_layout_alloc_from_plyt(r) ;
            gtk_widget_size_allocate(child, &alloc, baseline) ;
        } else {
            fprintf(stderr, "UNLAYOUTED HORIZONTAL HANDLE\n") ;
        }
    }
    // (Vertical) junction handles.
    for (i = 0; child && i < (njv) ;
         child = gtk_widget_get_next_sibling(child), ++ i)
    {
        if (child && gtk_widget_should_layout(child)) {
            // fprintf(stderr, "layouting vertical handle %d\n", i) ;
            plyt_pixels_rectangle r = plyt_abs_junction(self->layout, V, i) ;
            const GtkAllocation alloc = gtk_planche_layout_alloc_from_plyt(r) ;
            gtk_widget_size_allocate(child, &alloc, baseline) ;
        } else {
            fprintf(stderr, "UNLAYOUTED VERTICAL HANDLE\n") ;
        }
    }
}

static
void gtk_planche_layout_class_init(GtkPlancheLayoutClass *class) {
    GtkLayoutManagerClass* layout_manager_class = GTK_LAYOUT_MANAGER_CLASS(class) ;
    layout_manager_class->allocate = gtk_planche_layout_allocate ;
    layout_manager_class->measure  = gtk_planche_layout_measure ;
}

static
void gtk_planche_layout_init(GtkPlancheLayout* self) {
    self->layout = plyt_init() ;
    plyt_resize(self->layout, 1000, 400) ; // #fixme
    gtk_planche_layout_update_counters(self) ;
}

plyt_t* gtk_planche_layout_plyt(GtkPlancheLayout* self) {
    return self->layout ;
}

const plyt_t* gtk_planche_layout_const_plyt(const GtkPlancheLayout* self) {
    return self->layout ;
}

void gtk_planche_layout_update_counters(GtkPlancheLayout* self) {
    const uint njh = plyt_junctions_count(self->layout, H) ;
    const uint njv = plyt_junctions_count(self->layout, V) ;
    self->njunctions = njh + njv ;
    self->nchildren  = plyt_cells_count(self->layout) ;
    self->ncorners   = self->nchildren ;
}

GtkLayoutManager* gtk_planche_layout_new(void) {
    return g_object_new(GTK_TYPE_PLANCHE_LAYOUT, NULL) ;
}
