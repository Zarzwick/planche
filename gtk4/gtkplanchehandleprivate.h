#pragma once

#include <gtk/gtk.h>

#define GTK_TYPE_PLANCHE_HANDLE            (gtk_planche_handle_get_type())
#define GTK_PLANCHE_HANDLE(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GTK_TYPE_PLANCHE_HANDLE, GtkPlancheHandle))
#define GTK_PLANCHE_HANDLE_CLASS(class)    (G_TYPE_CHECK_CLASS_CAST((class), GTK_TYPE_PLANCHE_HANDLE, GtkPlancheHandleClass))
#define GTK_IS_PLANCHE_HANDLE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GTK_TYPE_PLANCHE_HANDLE))
#define GTK_IS_PLANCHE_HANDLE_CLASS(class) (G_TYPE_CHECK_CLASS_TYPE((class), GTK_TYPE_PLANCHE_HANDLE))
#define GTK_PLANCHE_HANDLE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GTK_TYPE_PLANCHE_HANDLE, GtkPlancheHandleClass))

typedef struct _GtkPlancheHandle      GtkPlancheHandle ;
typedef struct _GtkPlancheHandleClass GtkPlancheHandleClass ;
typedef enum   _GtkPlancheHandleType  GtkPlancheHandleType ;

enum _GtkPlancheHandleType {
	GTK_PLANCHE_HANDLE_HORIZONTAL,
	GTK_PLANCHE_HANDLE_VERTICAL,
	GTK_PLANCHE_HANDLE_CORNER
} ;

typedef struct _GtkPlanche GtkPlanche ;
typedef struct _GtkPlancheHandleCallbacks GtkPlancheHandleCallbacks ;

struct _GtkPlancheHandleCallbacks {
	void (*split)(GtkWidget* widget, axis_t axis, cid_t cell, pixels_t lim) ;
	bool (*prepare_move)(GtkWidget* widget, axis_t axis, cid_t j) ;
	void (*update_move)(GtkWidget* widget, axis_t axis, pixels_t p) ;
	void (*commit_move)(GtkWidget* widget, axis_t axis, pixels_t p) ;
} ;

/// A widget that will react to dragging events and use callbacks
/// provided in _GtkPlancheHandleCallbacks to affect the GtkPlanche
/// container.
struct _GtkPlancheHandle {
	GtkWidget parent_instance ;
	GtkPlancheHandleType type ;
	GtkPlancheHandleCallbacks* callbacks ;
	cid_t id ;
} ;

struct _GtkPlancheHandleClass {
	GtkWidgetClass parent_class ;
} ;

GType gtk_planche_handle_get_type(void) G_GNUC_CONST ;

GtkWidget* gtk_planche_handle_new(void) ;
