#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTK_TYPE_PLANCHE_LAYOUT (gtk_planche_layout_get_type ())

typedef struct plyt_t plyt_t ;

GDK_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE(GtkPlancheLayout, gtk_planche_layout, GTK, PLANCHE_LAYOUT, GtkLayoutManager)

GDK_AVAILABLE_IN_ALL
GtkLayoutManager* gtk_planche_layout_new(void) ;

/// Returns the plyt struct associated with the layout.
plyt_t* gtk_planche_layout_plyt(GtkPlancheLayout*) ;

/// Returns the plyt struct associated with the layout.
const plyt_t* gtk_planche_layout_const_plyt(const GtkPlancheLayout*) ;

/// This function is used to notify the layout that the number of
/// children, corners and junctions has changed. This is very niche.
void gtk_planche_layout_update_counters(GtkPlancheLayout*) ;

G_END_DECLS
