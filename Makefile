.PHONY: qt
.PHONY: gtk
.PHONY: doc
.PHONY: tests
.PHONY: clean

pikchr_files = $(wildcard doc/*.pikchr)
md_files     = $(wildcard doc/*.md)
svg_files    = $(pikchr_files:.pikchr=.svg)
html_files   = $(md_files:.md=.html)

all:

qt:
	mkdir -p build && cd build && qmake ..

gtk:
	cc -std=c99 -g -ggdb $(shell pkg-config --cflags gtk4) \
	src/planche.c gtk4/app.c \
	gtk4/gtkplanche.c gtk4/gtkplanchehandle.c gtk4/gtkplanchelayout.c \
	$(shell pkg-config --libs gtk4) -lc -lm -o app.bin

doc/%.svg: doc/%.pikchr
	@fossil pikchr $< $@

doc/%.html: doc/%.md
	@md2html --full-html $< -o $@

doc: $(svg_files) $(html_files)

tests:
	cc -std=c99 -lc -lm -g -ggdb src/tests.c -o tests

clean:
	@rm -f planche.o app.bin build/* doc/*.svg doc/*.html
