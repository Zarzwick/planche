#ifndef PLYT_C
#define PLYT_C

#include <math.h>
#include <string.h>

#include <stdio.h> // #fixme
#include <assert.h>

#include "planche.h"
#include "vec.incl"

#define OUTER_EDGE (1<<30)
#define PLACEHOLDERS_THRESHOLD (1<<20)
#define FP_SCALING (1<<16)

typedef plyt_pixels_rectangle rect2u_t ;
typedef plyt_pixels_rectangle rect2fp_t ;

struct junction_t { cid_t a ; cid_t b ; fixed_point_t p ; fixed_point_t s ; } ;
struct interval_t { fixed_point_t a ; fixed_point_t b ; cid_t c ; } ;

typedef fixed_point_t fp_t ;

// Utils.

/*inline*/ int max(int a, int b) { return (a > b) ? a : b ; } ;
/*inline*/ int min(int a, int b) { return (a < b) ? a : b ; } ;

/*inline*/ int compare_intervals(const interval_t a, const interval_t b) {
	return (a.a < b.a) || ((a.a == b.a) && (a.b < b.b)) ;
}

/*inline*/ fixed_point_t from_unit_float(float f) {
	return (fixed_point_t)(floorf(f * FP_SCALING)) ;
}

float to_unit_float(fixed_point_t fp) {
	return (fp != 0) ? (((float) fp) / FP_SCALING) : 0.0 ;
}

/*inline*/ pixels_t abs_from_rel(fixed_point_t p, pixels_t total) {
	return (p * total) / FP_SCALING ;
}

/*inline*/ axis_t opposite(axis_t a) {
	return (a == H) ? V : H ;
}

/// When a resizing occurs, relevant junctions that must be modified are
/// retrieved and stored in this structure, alongside the interval in which
/// the current set of junctions can be moved. I'm afraid the code looks like
/// so many other mesh processing algorithms, with few letters variables :>
typedef struct move_context_t {
	vec_t ca ; // [id_t]
	vec_t cb ; // [id_t]
	vec_t suggestions ; // [pixels_t]
	pixels_t lim_a ; // = 0 ;
	pixels_t lim_b ; // = 0 ;
	uint au, av ;
	uint bu, bv ;
	// bool a_outer ;
	// bool b_outer ;
} move_ctx_t ;

/// ...
typedef struct drop_context_t {
	size_t start ;
	size_t cell ;
	axis_t axis ;
	fp_t split ;
	bool side_a ;
	bool dirty ;
	bool swap ;
} drop_ctx_t ;

/// Attributes of a cell, telling whether they expand, fill, etc...
typedef struct size_constraints_t {
	int expand ; // = 0 ;
	int fill ; // = 0 ;
} size_constraints_t ;

/// The main type of that code. This is what you should have
/// somewhere in your calling library. Encapsulation is not my creed
/// so you're expected not to mess with things inside it :>
struct plyt_t {
	// Cells.
	vec_t cells ; // [rect2fp_t]
	vec_t cells_sizes ; // [rect2u_t]
	// The cells array contains **both** true cells and placeholder
	// cells, that are used to compute the junctions more easily.
	// Put differently, nc <= cells.items.len, and the nc first cells
	// are the actual cells.
	cid_t nc ;
	// // Constraints.
	// std::vector<SizeConstraints> cells_constraints ;
	int inter_cell_gap ; // = 1
	int intra_cell_margin ; // = 2
	uint size[2] ;
	// Junctions per axis (dependant data).
	vec_t junctions[2] ; // [junction_t]
	// And some useful other informations.
	//uint lines_junctions_thickness = 10 ; // In pixels.
	move_ctx_t move_ctx ;
	drop_ctx_t drop_ctx ;
} ;

plyt_t* plyt_init() {
	plyt_t* plyt = malloc(sizeof(plyt_t)) ;
	plyt->inter_cell_gap = 1 ;
	plyt->intra_cell_margin = 2 ;
	// Initialise vectors.
	plyt->cells        = vec_init_capacity(sizeof(rect2fp_t), 8) ;
	plyt->cells_sizes  = vec_init_capacity(sizeof(rect2u_t),  8) ;
	plyt->junctions[0] = vec_init_capacity(sizeof(junction_t), 8) ;
	plyt->junctions[1] = vec_init_capacity(sizeof(junction_t), 8) ;
	plyt->move_ctx.ca  = vec_init_capacity(sizeof(cid_t), 8) ;
	plyt->move_ctx.cb  = vec_init_capacity(sizeof(cid_t), 8) ;
	plyt->move_ctx.suggestions = vec_init_capacity(sizeof(pixels_t), 8) ;
	plyt->nc = 1 ;
	// Initialise the root cell.
	const fixed_point_t one = from_unit_float(1.0) ;
	const rect2u_t cell = {{0, 0}, {one, one}} ;
	rect2fp_t undefined_size ;
	vec_append(&plyt->cells, &cell) ;
	vec_append(&plyt->cells_sizes, &undefined_size) ;
	return plyt ;
}

void plyt_deinit(plyt_t* plyt) {
	vec_free(&plyt->cells) ;
	vec_free(&plyt->cells_sizes) ;
	vec_free(&plyt->junctions[0]) ;
	vec_free(&plyt->junctions[1]) ;
	vec_free(&plyt->move_ctx.ca) ;
	vec_free(&plyt->move_ctx.cb) ;
	vec_free(&plyt->move_ctx.suggestions) ;
	free(plyt) ;
}

void plyt_add_junction(
	plyt_t* plyt,
	axis_t axis,
	cid_t a,
	cid_t b,
	fixed_point_t p,
	fixed_point_t s
) {
	const junction_t j = {a, b, p, s} ;
	vec_append(&plyt->junctions[axis], &j) ;
}

void plyt_recompute_junctions(plyt_t* plyt) {
	// fprintf(stderr, "-------------\n") ;
	int counter[2] = {0, 0} ;
	vec_clear(&plyt->junctions[H]) ;
	vec_clear(&plyt->junctions[V]) ;
	const char plyt_edge_none = 0 ;
	const size_t nc = plyt->nc ;
	const size_t np = plyt->cells.items.len ;
	for (uint i = 0; i < nc; ++ i) {
		const rect2fp_t ci = vec_get(&plyt->cells, i, rect2fp_t) ;
		const fp_t iax = ci.pos[H] ;
		const fp_t iay = ci.pos[V] ;
		const fp_t ibx = ci.size[H] + iax ;
		const fp_t iby = ci.size[V] + iay ;
		if (iax == 0) {
			plyt_add_junction(plyt, V, OUTER_EDGE, i, iay, ci.size[V]) ;
			counter[V] ++ ;
		}
		if (ibx == FP_SCALING) {
			plyt_add_junction(plyt, V, i, OUTER_EDGE, iay, ci.size[V]) ;
			counter[V] ++ ;
			// fprintf(stderr, "Add trailing vertical junction %d\n", counter[V]) ;
		}
		if (iay == 0) {
			plyt_add_junction(plyt, H, OUTER_EDGE, i, iax, ci.size[H]) ;
			counter[H] ++ ;
		}
		if (iby == FP_SCALING) {
			plyt_add_junction(plyt, H, i, OUTER_EDGE, iax, ci.size[H]) ;
			// fprintf(stderr, "Add trailing horizontal junction %d\n", counter[H]) ;
			counter[H] ++ ;
		}
		for (uint j = i+1; j < plyt->cells.items.len; ++ j) {
			const rect2fp_t cj = vec_get(&plyt->cells, j, rect2fp_t) ;
			const fp_t jax = cj.pos[H] ;
			const fp_t jay = cj.pos[V] ;
			const fp_t jbx = cj.size[H] + jax ;
			const fp_t jby = cj.size[V] + jay ;
			const fp_t maxax = max(iax, jax) ;
			const fp_t maxay = max(iay, jay) ;
			const fp_t minbx = min(ibx, jbx) ;
			const fp_t minby = min(iby, jby) ;
			if ((maxax == minbx) && (maxay < minby)) {
				const fp_t ca = (iax < jax) ? i : j ;
				const fp_t cb = i + j - ca ;
				plyt_add_junction(plyt, V, ca, cb, maxay, minby - maxay) ;
				counter[V] ++ ;
			} else if ((maxay == minby) && (maxax < minbx)) {
				const fp_t ca = (iay < jay) ? i : j ;
				const fp_t cb = i + j - ca ;
				plyt_add_junction(plyt, H, ca, cb, maxax, minbx - maxax) ;
				counter[H] ++ ;
			}
		}
	}
}

void plyt_recompute_size(plyt_t* plyt, cid_t c) {
	const int offset = plyt->intra_cell_margin + plyt->inter_cell_gap ;
	const rect2fp_t* cl = vec_get_ptr(&plyt->cells, c, rect2fp_t) ;
	rect2u_t* cs = vec_get_ptr(&plyt->cells_sizes, c, rect2u_t) ;
	cs->pos[X]   = abs_from_rel(cl->pos[X],  plyt->size[H]) + offset ;
	cs->pos[V]   = abs_from_rel(cl->pos[V],  plyt->size[V]) + offset ;
	cs->size[X]  = abs_from_rel(cl->size[X], plyt->size[H]) - 2 * offset ;
	cs->size[V]  = abs_from_rel(cl->size[V], plyt->size[V]) - 2 * offset ;
}

void plyt_recompute_sizes(plyt_t* plyt) {
	const int offset = plyt->intra_cell_margin + plyt->inter_cell_gap ;
	// fprintf(stderr, "[plyt_recompute_sizes] offset: %d\n", offset) ;
	for (uint c = 0; c < plyt->cells.items.len; ++ c) {
		const rect2fp_t* cl = vec_get_ptr(&plyt->cells, c, rect2fp_t) ;
		rect2u_t* cs = vec_get_ptr(&plyt->cells_sizes, c, rect2u_t) ;
		cs->pos[X]   = abs_from_rel(cl->pos[X],  plyt->size[H]) + offset ;
		cs->pos[V]   = abs_from_rel(cl->pos[V],  plyt->size[V]) + offset ;
		cs->size[X]  = abs_from_rel(cl->size[X], plyt->size[H]) - 2 * offset ;
		cs->size[V]  = abs_from_rel(cl->size[V], plyt->size[V]) - 2 * offset ;
	}
}

void plyt_resize(plyt_t* plyt, size_t w, size_t h) {
	plyt->size[H] = w ;
	plyt->size[V] = h ;
	fprintf(stderr, "resizing to %zux%zu\n", w, h) ;
	// Affect cells *relative* sizes to solve constraints.
	// #todo #constraints
	// Recompute the *absolute* sizes, and *relative* junctions.
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
}

void plyt_split(
	plyt_t* plyt,
	axis_t axis,
	cid_t c,
	pixels_t p
) {
	rect2fp_t* cell = vec_get_ptr(&plyt->cells, c, rect2fp_t) ;
	const fp_t p_ratio = (p * FP_SCALING) / plyt->size[axis] ;
	const fp_t s = p_ratio - cell->pos[axis] ;
	const fp_t t = cell->size[axis] - s ;
	rect2fp_t nc = *cell ; // copy.
	nc.pos[axis] += s ;
	nc.size[axis] = t ;
	cell->size[axis] = s ;
	rect2u_t undefined_size = {{0, 0}, {0, 0}} ;
	vec_append(&plyt->cells, &nc) ;
	vec_append(&plyt->cells_sizes, &undefined_size) ;
	plyt->nc ++ ;
	// cells_constraints.emplace_back() ;
	//recompute_size(c) ;
	//recompute_size(cells.size()-1) ;
	//recompute_junctions() ;
}

void plyt_merge(
	plyt_t* plyt,
	axis_t axis,
	cid_t j,
	bool a_into_b
) {
	const axis_t oppo = opposite(axis) ;
	const junction_t j_oppo = vec_get(&plyt->junctions[oppo], j, junction_t) ;
	const cid_t ca = j_oppo.a ;
	const cid_t cb = j_oppo.b ;
	// // Check if the merge is possible.
	// const auto bad_pos = (cells[ca].pos[oppo] != cells[cb].pos[oppo]) ;
	// const auto bad_size = (cells[ca].size[oppo] != cells[cb].size[oppo]) ;
	// if (bad_pos or bad_size)
	// 	return ;
	// Resize the 'a' cell.
	rect2fp_t* cell_a = vec_get_ptr(&plyt->cells, ca, rect2fp_t) ;
	rect2fp_t* cell_b = vec_get_ptr(&plyt->cells, cb, rect2fp_t) ;
	const fp_t total = cell_a->size[axis] + cell_b->size[axis] ;
	fprintf(stderr, "%f + %f = %f\n",
		to_unit_float(cell_a->size[axis]),
		to_unit_float(cell_b->size[axis]),
		to_unit_float(total)) ;
	cell_a->size[axis] = total ;
	cell_b->size[axis] = total ;
	// Remove the 'b' cell.
	const cid_t co = a_into_b ? ca : cb ;
	vec_remove(&plyt->cells, cb) ;
	vec_remove(&plyt->cells_sizes, cb) ;
	plyt->nc -= 1 ;
	// vec_remove(&plyt->constraints, cb) ; // #todo #constraints
	//recompute_size(ca) ;
	//recompute_junctions() ;
}

static
void plyt_swap_cells(plyt_t* plyt, cid_t a, cid_t b) {
	const rect2fp_t ca = vec_get(&plyt->cells, a, rect2fp_t) ;
	const rect2fp_t cb = vec_get(&plyt->cells, b, rect2fp_t) ;
	vec_set(&plyt->cells, a, &cb) ;
	vec_set(&plyt->cells, b, &ca) ;
}

// static
// void plyt_clear_junction_placeholder(plyt_t* plyt, axis_t axis, cid_t j) {
// 	const junction_t* junct = vec_get_ptr(&plyt->junctions[axis], j, junction_t) ;
// 	const rect2fp_t* ca = vec_get_ptr(&plyt->cells, j->a, rect2fp_t) ;
// 	const rect2fp_t* cb = vec_get_ptr(&plyt->cells, j->b, rect2fp_t) ;
// 	const rect2fp_t* pl = (j->a < plyt->nc) ? ((j->b < plyt->nc) ? NULL : cb) : ca ;
// 	if (pl && (pl->size[opposite(axis)] == 0))
// 		plyt_merge(plyt, axis, j) ;
// }

/// This function has a few loops, but it should not be called a lot
/// anyway, so I think it's okay. This is a special case.
static
void plyt_clear_degenerate_placeholders(plyt_t* plyt) {
	const uint l = plyt->cells.items.len ;
	for (uint i = plyt->nc; i < l; ++ i) {
		const rect2fp_t* c = vec_get_ptr(&plyt->cells, i, rect2fp_t) ;
		if ((c->size[X] == 0) || (c->size[Y] == 0)) {
			axis_t axis = (c->size[X] == 0) ? Y : H ;
			uint j = 0 ;
			bool a_into_b = false ;
			bool junction_found = false ;
			for (; plyt->junctions[axis].items.len; ++ j) {
				const junction_t* junct = vec_get_ptr(&plyt->junctions[axis], j, junction_t) ;
				if ((junct->a == i) || (junct->b == i)) {
					a_into_b = (i == junct->a) ;
					junction_found = true ;
					break ;
				}
			}
			// assert(junction_found) ;
			plyt_merge(plyt, opposite(axis), j, a_into_b) ;
			plyt->nc ++ ;
		}
	}
}

// #fixme rename
typedef struct {
	plyt_t* plyt ;
	axis_t oppo ;
} plyt_junction_cmp_data ;

int plyt_junction_cmp(
	const void* a_opaque,
	const void* b_opaque,
	void* data_opaque
) {
	cid_t a = *(cid_t*)(a_opaque) ;
	cid_t b = *(cid_t*)(b_opaque) ;
	plyt_junction_cmp_data* data =
		(plyt_junction_cmp_data*) data_opaque ;
	const rect2fp_t* cells = data->plyt->cells.items.ptr ;
	return cells[a].pos[data->oppo] < cells[b].pos[data->oppo] ;
}


#include "sort.c"  // fixme

bool plyt_prepare_move(
	plyt_t* plyt,
	axis_t axis,
	cid_t junction
) {
	move_ctx_t* ctx = &plyt->move_ctx ;
	const axis_t oppo = opposite(axis) ;
	junction_t* j = vec_get_ptr(&plyt->junctions[oppo], junction, junction_t) ;
	// std::cerr << "- Junction asked joins (" << j->a << ", " << j->b << ")" << std::endl ;
	const bool a_outer = (j->a == OUTER_EDGE) ;
	const bool b_outer = (j->b == OUTER_EDGE) ;
	// Careful ! I use this pointer as a shorthand to avoid my
	// vec_get... Note that this works because no cells are added
	// during the function. Please be careful, still.
	rect2fp_t* cells = (rect2fp_t*) plyt->cells.items.ptr ;
	// If we are moving an outer edge, we must add a placeholder cell
	// that will make computating junctions much easier. In addition
	// to splitting, we must ensure that the placeholder cell is
	// after plyt->nc (and that the visible cell remain the same).
	if (a_outer || b_outer) {
		const cid_t s = a_outer ? j->b : j->a ;
		const rect2u_t cs = vec_get(&plyt->cells_sizes, s, rect2u_t) ;
		const pixels_t ps = cs.pos[axis] + cs.size[axis] / 2 ;
		plyt_split(plyt, axis, s, ps) ;
		const cid_t t = plyt->cells.items.len - 1 ;
		plyt->nc -- ;
		if (a_outer)
			plyt_swap_cells(plyt, s, t) ;
		plyt_recompute_sizes(plyt) ; // #useless?
		plyt_recompute_junctions(plyt) ;
		for (uint k = 0; k < plyt_junctions_count(plyt, oppo); ++ k) {
			junction_t* jk = vec_get_ptr(&plyt->junctions[oppo], k, junction_t) ;
			if (((jk->a == s) && (jk->b == t)) ||
				((jk->a == t) && (jk->b == s))) {
				junction = k ;
				j = vec_get_ptr(&plyt->junctions[oppo], junction, junction_t) ;
			}
		}
	}
	// This is the position of the junction on the opposite axis.
	const fp_t p = a_outer ?
		cells[j->b].pos[axis] :
		cells[j->a].pos[axis] + cells[j->a].size[axis] ;
	//std::cerr << "- Trying to move edge at position " ; show_fp(p) ; std::cerr << std::endl ;
	vec_clear(&ctx->ca) ;
	vec_clear(&ctx->cb) ;
	vec_ensure_capacity(&ctx->ca, plyt->cells.items.len) ;
	vec_ensure_capacity(&ctx->cb, plyt->cells.items.len) ;
	for (uint i = 0; i < plyt->cells.items.len; ++ i) {
		const fp_t a = cells[i].pos[axis] ;
		const fp_t b = cells[i].pos[axis] + cells[i].size[axis] ;
		if (a == p)	vec_append(&ctx->cb, &i) ;
		if (b == p)	vec_append(&ctx->ca, &i) ;
	}
	// Intervals on each side of the edge are sorted along the edge.
	// This makes it "easy" to find the region we're looking for.
	for (uint i = 0; i < ctx->ca.items.len; ++ i) fprintf(stderr, "[a] %d (%p)\n", vec_get(&ctx->ca, i, cid_t), vec_get_ptr(&ctx->ca, i, cid_t)) ;
	for (uint i = 0; i < ctx->cb.items.len; ++ i) fprintf(stderr, "[b] %d (%p)\n", vec_get(&ctx->cb, i, cid_t), vec_get_ptr(&ctx->cb, i, cid_t)) ;
	plyt_junction_cmp_data data = {plyt, oppo} ;
	// qsort_r(
	selection_sort(
		ctx->ca.items.ptr,
		ctx->ca.items.len,
		ctx->ca.itemsize,
		plyt_junction_cmp,
		&data
	) ;
	// qsort_r(
	selection_sort(
		ctx->cb.items.ptr,
		ctx->cb.items.len,
		ctx->cb.itemsize,
		plyt_junction_cmp,
		&data
	) ;
	// Look for j->a while maintaining continuity. Index u represents
	// the first interval (or cell) in ivals_a that is know to be
	// "reachable" from j.
	const uint lim = ctx->ca.items.len ;
	uint i = 0 ;
	// uint ua = 0 ;
	while ((i < lim - 1) && (vec_get(&ctx->ca, i, cid_t) != j->a)) {
		const rect2fp_t* curr = &cells[vec_get(&ctx->ca, i, cid_t)] ;
		const rect2fp_t* next = &cells[vec_get(&ctx->ca, i+1, cid_t)] ;
		const fp_t curr_end = curr->pos[oppo] + curr->size[oppo] ;
		const fp_t next_begin = next->pos[oppo] ;
		i += 1 ;
		if (curr_end < next_begin)
			ctx->au = i ;
	}
	// Now i is necessarily on the correct interval. So we can look for
	// the end of the chain with a second index v.
	// uint va = i ;
	while (i < lim - 1) {
		const rect2fp_t* curr = &cells[vec_get(&ctx->ca, i, cid_t)] ;
		const rect2fp_t* next = &cells[vec_get(&ctx->ca, i+1, cid_t)] ;
		const fp_t curr_end = curr->pos[oppo] + curr->size[oppo] ;
		const fp_t next_begin = next->pos[oppo] ;
		if (curr_end < next_begin) {
			ctx->av = i ;
			break ;
		}
		++ i ;
	}
	if (i == lim - 1) {
		ctx->av = i ;
	}
	const fp_t bound_u = cells[vec_get(&ctx->ca, ctx->au, cid_t)].pos[oppo] ;
	const fp_t bound_v = cells[vec_get(&ctx->ca, ctx->av, cid_t)].pos[oppo] +
					     cells[vec_get(&ctx->ca, ctx->av, cid_t)].size[oppo] ;
	// std::cerr << "- Bound u: " ; show_fp(bound_u) ; std::cerr << std::endl ;
	// std::cerr << "- Bound v: " ; show_fp(bound_v) ; std::cerr << std::endl ;
	// uint ub = 0, vb = 0 ;
	// Now the correspondance in ivals_b can be found easily.
	for (i = 0; i < ctx->cb.items.len; ++ i) {
		const rect2fp_t* ci = &cells[vec_get(&ctx->cb, i, cid_t)] ;
		if (ci->pos[oppo] == bound_u) ctx->bu = i ;
		if (ci->pos[oppo] + ci->size[oppo] == bound_v) ctx->bv = i ;
	}
	if (0) {
		// Now, if needed, we can trim the selection to stop at junctions with
		// valence 4. This is a matter of intuition for the user.
		// TODO
	}
	// std::cerr << "- Indices au: " << move_ctx.au << ", av: " << move_ctx.av << std::endl ;
	// std::cerr << "- Indices bu: " << move_ctx.bu << ", bv: " << move_ctx.bv << std::endl ;
	ctx->lim_a = 0 ;
	ctx->lim_b = plyt->size[axis] ;
	// Compute the interval in which the full edge can be moved.
	for (i = ctx->au; i <= ctx->av; ++ i) {
		const cid_t k = vec_get(&ctx->ca, i, cid_t) ;
		const fp_t cell_lim = vec_get(&plyt->cells_sizes, k, rect2u_t).pos[axis] ;
		ctx->lim_a = max(ctx->lim_a, cell_lim) ;
	}
	for (i = ctx->bu; i <= ctx->bv; ++ i) {
		const cid_t k = vec_get(&ctx->cb, i, cid_t) ;
		const rect2u_t s = vec_get(&plyt->cells_sizes, k, rect2u_t) ;
		const fp_t cell_lim = s.pos[axis] + s.size[axis] ;
		ctx->lim_b = min(ctx->lim_b, cell_lim) ;
	}
	if (0) { // #fixme #useless?
		ctx->lim_a += (plyt->inter_cell_gap + plyt->intra_cell_margin + 1) ;
		ctx->lim_b -= (plyt->inter_cell_gap + plyt->intra_cell_margin + 1) ;
	}
	if (ctx->lim_a >= ctx->lim_b)
		return false ;
	// std::cerr << "- Lim. a: " << move_ctx.lim_a << std::endl ;
	// std::cerr << "- Lim. b: " << move_ctx.lim_b << std::endl ;
	// Find adjacent cells to propose. 'eu' and 'ev' are *edge* [u, v].
	vec_clear(&ctx->suggestions) ;
	const fp_t eu = cells[vec_get(&ctx->ca, ctx->au, cid_t)].pos[oppo] ;
	const fp_t ev = cells[vec_get(&ctx->ca, ctx->av, cid_t)].pos[oppo] +
				    cells[vec_get(&ctx->ca, ctx->av, cid_t)].size[oppo] ;
	// std::cerr << "- Edge u: " ; show_fp(eu) ; std::cerr << std::endl ;
	// std::cerr << "- Edge v: " ; show_fp(ev) ; std::cerr << std::endl ;
	for (uint i = 0; i < plyt->cells.items.len; ++ i) {
		if (cells[i].pos[axis] != 0) {
			const fp_t cu = cells[i].pos[oppo] ;
			const fp_t cv = cells[i].pos[oppo] + cells[i].size[oppo] ;
			//std::cerr << "- Cell u: " ; show_fp(cu) ; std::cerr << std::endl ;
			//std::cerr << "- Cell v: " ; show_fp(cv) ; std::cerr << std::endl ;
			if ((cu == ev) || (cv == eu)) {
				//std::cerr << "Suggest !" << std::endl ;
				const pixels_t tmp = vec_get(&plyt->cells_sizes, i, rect2u_t).pos[axis] ;
				const pixels_t sug = tmp - plyt->intra_cell_margin - plyt->inter_cell_gap ;
				vec_append(&ctx->suggestions, &sug) ;
			}
		}
	}
	// std::cerr << "Suggestions:" << std::endl ;
	// for (const auto suggestion: move_ctx.suggestions) {
	// 	std::cerr << "- " << suggestion << std::endl ;
	// }
	return true ;
}

pixels_t plyt_control_move_and_suggest(
	plyt_t* plyt,
	axis_t axis,
	pixels_t p
) {
	fprintf(stderr, "move'n'suggest %zu\n", p) ;
	move_ctx_t* ctx = &plyt->move_ctx ;
	const pixels_t clamped = max(ctx->lim_a, min(p, ctx->lim_b)) ;
	if (clamped != p) {
		fprintf(stderr, "clamped %f -> %f (axis %d)\n",
			to_unit_float(p), to_unit_float(clamped), axis) ;
		return clamped ;
	}
	int best_sug ;
	int min_dist = plyt->size[axis] ;
	for (uint i = 0; i < ctx->suggestions.items.len; ++ i) {
		const pixels_t sug = vec_get(&ctx->suggestions, i, pixels_t) ;
		if ((sug < ctx->lim_a) || (sug > ctx->lim_b))
			continue ;
		const pixels_t dist = abs(sug - clamped) ;
		if (min_dist > dist) {
			min_dist = dist ;
			best_sug = sug ;
		}
	}
	return (min_dist < 10) ? best_sug : clamped ;
}

void plyt_move(
	plyt_t* plyt,
	axis_t axis,
	pixels_t p
) {
	move_ctx_t* ctx = &plyt->move_ctx ;
	const fp_t ratio = (p * FP_SCALING) / plyt->size[axis] ;
	for (uint i = ctx->au; i <= ctx->av; ++ i) {
		const cid_t ca = vec_get(&ctx->ca, i, cid_t) ;
		rect2fp_t* cell_a = vec_get_ptr(&plyt->cells, ca, rect2fp_t) ;
		cell_a->size[axis] = ratio - cell_a->pos[axis] ;
	}
	for (uint i = ctx->bu; i <= ctx->bv; ++ i) {
		const cid_t cb = vec_get(&ctx->cb, i, cid_t) ;
		rect2fp_t* cell_b = vec_get_ptr(&plyt->cells, cb, rect2fp_t) ;
		cell_b->size[axis] = cell_b->pos[axis] + cell_b->size[axis] - ratio ;
		cell_b->pos[axis] = ratio ;
	}
	plyt_clear_degenerate_placeholders(plyt) ;
}

size_t plyt_cells_count(const plyt_t* plyt) {
	return plyt->nc ;
}

size_t plyt_junctions_count(const plyt_t* plyt, axis_t axis) {
	return plyt->junctions[axis].items.len ;
}

plyt_pixels_rectangle plyt_abs_cell(const plyt_t* plyt, cid_t c) {
	const rect2u_t* cell = vec_get_ptr(&plyt->cells_sizes, c, rect2u_t) ;
	plyt_pixels_rectangle rect ;
	rect = *cell ;
	return rect ;
}

plyt_pixels_rectangle plyt_abs_junction(const plyt_t* plyt, axis_t axis, int j) {
	// #fixme #constant #magic
	const int half_thickness = 20 ;
	const int corners = 20 ;
	const junction_t* junct = vec_get_ptr(&plyt->junctions[axis], j, junction_t) ;
	const axis_t oppo = opposite(axis) ;
	const pixels_t axis_p = abs_from_rel(junct->p, plyt->size[axis]) + corners;
	const pixels_t axis_s = abs_from_rel(junct->s, plyt->size[axis]) - corners * 2 ;
	const pixels_t oppo_s = half_thickness * 2 ;
	pixels_t oppo_p ;
	if (junct->a == OUTER_EDGE) {
		const rect2fp_t* cb = vec_get_ptr(&plyt->cells, junct->b, rect2fp_t) ;
		oppo_p = abs_from_rel(cb->pos[oppo], plyt->size[oppo]) - half_thickness ;
	} else {
		const rect2fp_t* ca = vec_get_ptr(&plyt->cells, junct->a, rect2fp_t) ;
		oppo_p = abs_from_rel(ca->pos[oppo] + ca->size[oppo], plyt->size[oppo]) - half_thickness ;
	}
	plyt_pixels_rectangle r ;
	if (axis == V)
		r = (plyt_pixels_rectangle) {{oppo_p, axis_p}, {oppo_s, axis_s}} ;
	else
		r = (plyt_pixels_rectangle) {{axis_p, oppo_p}, {axis_s, oppo_s}} ;
	return r ;
}

plyt_unit_rectangle plyt_rel_cell(const plyt_t* plyt, cid_t c) {
	const rect2fp_t* cell = vec_get_ptr(&plyt->cells, c, rect2fp_t) ;
	plyt_unit_rectangle rect ;
	rect.pos[H]  = to_unit_float(cell->pos[H]) ;
	rect.pos[V]  = to_unit_float(cell->pos[V]) ;
	rect.size[H] = to_unit_float(cell->size[H]) ;
	rect.size[H] = to_unit_float(cell->size[V]) ;
	return rect ;
}

// plyt_unit_rectangle plyt_rel_junction(const plyt_t* plyt, axis_t axis, int j) {
// 	// #todo #fixme
// }

/// Something to put on a stack, for depth-first traversal.
typedef struct cell_triple_t {
	cid_t id ;
	pixels_t mins ;
	pixels_t defs ;
} cell_triple_t ;

/// Returns true if there is an edge a|b (but not b|a). The parameter
/// axis is the opposite of the major dimension of the junction.
/// #fixme #axis this arises from an ambiguity from early days.
static
bool plyt_connected(plyt_t* plyt, axis_t axis, cid_t a, cid_t b) {
	const axis_t oppo = opposite(axis) ;
	const size_t nj = plyt_junctions_count(plyt, oppo) ;
	for (size_t j = 0; j < nj; ++ j) {
		junction_t* ptr = vec_get_ptr(&plyt->junctions[oppo], j, junction_t) ;
		if ((a == ptr->a) && (b == ptr->b))
			return true ;
	}
	return false ;
}

#define PLYT_WORK_AROUND_CONSTRAINTS

void plyt_min_and_default_sizes(
	plyt_t* plyt,
	axis_t axis,
	pixels_t* mins,
	pixels_t* defs,
	fetch_op fetch,
	void* fetch_data,
	fold_op fold,
	void* fold_data
) {
	const size_t nc = plyt_cells_count(plyt) ;
#ifdef PLYT_WORK_AROUND_CONSTRAINTS
	// This is a workaround to my code lacking a proper LP solver.
	// Because the minimum size, as it is defined, assume that all
	// relative sizes are optimal, they should be adapted using a
	// set of constraints. This involves either incompatible licensed
	// code or me coding the simplex algorithm, and I'm lazy. In
	// order to compensate this, a conservative minimum size is also
	// computed : this size S is s.t., for all cell C,
	// (S * C.rel) > C.min. This is a #todo.
	// C.min / C.rel < S
	pixels_t conservative_mins = 0 ;
	for (size_t i = 0; i < nc; i++) {
		pixels_t a, b ;
		fetch(i, &a, &b, fetch_data) ;
		const rect2fp_t* c = vec_get_ptr(&plyt->cells, i, rect2fp_t) ;
		const pixels_t cmins = (a * FP_SCALING) / c->size[axis] ;
		conservative_mins = max(conservative_mins, cmins) ;
	}
#endif
	*mins = 0 ;
	*defs = 0 ;
	cell_triple_t triple ;
	// Initialise a stack (lol, a vector and some int...). On the top
	// of this stack, put the "outer" cell, so that all children are
	// the topmost or leftmost cells.
	vec_t unvisited = vec_init_capacity(sizeof(cell_triple_t), nc) ;
	int sp = 0 ; // the stack pointer...
	triple.id = OUTER_EDGE ;
	triple.mins = 0 ;
	triple.defs = 0 ;
	vec_append(&unvisited, &triple) ;
	// Start the DFS. At each leaf, affect the mins and defs.
	while (unvisited.items.len > 0) {
		cell_triple_t top = vec_get(&unvisited, sp, cell_triple_t) ;
		vec_remove(&unvisited, sp--) ; // pop.
		// Detect all children, that is, cells that are connected to
		// the right/bottom of the current cell. And then stack them.
		int nchildren = 0 ;
		for (size_t i = 0; i < nc; i++) {
			if (plyt_connected(plyt, axis, top.id, i)) {
				fetch(i, &triple.mins, &triple.defs, fetch_data) ;
				triple.id = i ;
				triple.mins += top.mins ;
				triple.defs += top.defs ;
				vec_append(&unvisited, &triple) ;
				nchildren++ ;
				sp++ ;
			}
		}
		// If it is a leaf, affect container sizes.
		if (nchildren == 0) {
			*mins = max(*mins, top.mins) ;
			*defs = fold(*defs, top.defs, fold_data) ;
		}
	}
#ifdef PLYT_WORK_AROUND_CONSTRAINTS
	*mins = max(*mins, conservative_mins) ;
#endif
	*defs = max(*defs, *mins) ;
	vec_free(&unvisited) ;
}

// ///
// /// This functions assume that the size of the layout is already
// /// greater or equal to the minimum size computed by the (relatively)
// /// expensive plyt_min_and_default_sizes().
// ///
// void plyt_solve_rel_sizes(
// 	plyt_t* plyt,
// 	axis_t axis,
// 	fetch_op fetch,
// 	void* fetch_data
// ) {
// 	// ...
// }

/// From a relative coordinate, propose an operation (split, replace,
/// ...) that would seem the more natural for a user. A good source
/// of inspiration is the way swaywm handles it.
void plyt_propose_drop_operation(plyt_t* plyt, fp_t rx, fp_t ry) {
	const size_t nc = plyt_cells_count(plyt) ;
	fp_t dax, day, dbx, dby ;
	rect2fp_t cell ;
	// Find the cell we are in.
	// if (!plyt->drop.dirty) {
	// 	// ...
	// }
	size_t i ;
	for (i = 0; i < nc; ++ i) {
		cell = vec_get(&plyt->cells, i, rect2fp_t) ;
		dax = rx - cell.pos[0] ;
		day = ry - cell.pos[1] ;
		dbx = cell.pos[0] + cell.size[0] - rx ;
		dby = cell.pos[1] + cell.size[1] - ry ;
		if ((dax >= 0) && (dbx >= 0) && (day >= 0) && (dby >= 0)) {
			break ;
		}
	}
	// Just check something...
	if (i == plyt->drop_ctx.start) {
		plyt->drop_ctx.dirty = true ;
		return ;
	}
	// Find the closest junction, if applicable. In this part cax is
	// "close from edge a on axis x". There are 9 possible cases,
	// that respect the following layout:
	// 4 3 5
	// 1 0 2
	// 7 6 8
	const fp_t lim = from_unit_float(0.1) ;
	int ca[2], cb[2] ;
	ca[H] = (dax < lim) ? 1 : 0 ;
	ca[V] = (day < lim) ? 3 : 0 ;
	cb[H] = (dbx < lim) ? 2 : 0 ;
	cb[V] = (dby < lim) ? 6 : 0 ;
	const int sum = ca[H] + ca[V] + cb[H] + cb[V] ;
	// assert(xor(cax, cbx))
	// assert(xor(cay, cby))
	// Now, fill in the drop structure. If not marked dirty, the
	// structure carries information from a previous query (since a
	// user is intended to drag the cursor, there is one query per
	// frame).
	plyt->drop_ctx.cell = i ;
	plyt->drop_ctx.swap = (sum == 0) ;
	switch (sum) {
		case 0: break ;
		case 1: case 2: plyt->drop_ctx.axis = H ; break ;
		case 3: case 6: plyt->drop_ctx.axis = V ; break ;
		default:
			// This a very niche case, it should not happen often
			// except if the limit is relatively large.
			if (plyt->drop_ctx.dirty)
				plyt->drop_ctx.axis = (plyt->size[H] < plyt->size[V]) ? V : H ;
	}
	const axis_t axis = plyt->drop_ctx.axis ;
	plyt->drop_ctx.split = cell.pos[axis] + (cell.size[axis] / 2) ;
	plyt->drop_ctx.side_a = (ca[axis] != 0) ;
	plyt->drop_ctx.dirty = false ;
}

/// This applies the operations proposed earlier. There is no exact
/// constraint on how deeply the layout could be modified.
void plyt_apply_drop_operation(plyt_t* plyt) {
	if (plyt->drop_ctx.dirty)
		return ;
	if (plyt->drop_ctx.swap) {
		const rect2fp_t a = vec_get(&plyt->cells, plyt->drop_ctx.start, rect2fp_t) ;
		const rect2fp_t b = vec_get(&plyt->cells, plyt->drop_ctx.cell, rect2fp_t) ;
		vec_set(&plyt->cells, plyt->drop_ctx.start, &b) ;
		vec_set(&plyt->cells, plyt->drop_ctx.cell, &a) ;
		return ;
	}
	// Affect the target and start cell rectangles.
	const axis_t axis = plyt->drop_ctx.axis ;
	const rect2fp_t o = vec_get(&plyt->cells, plyt->drop_ctx.cell, rect2fp_t) ;
	rect2fp_t a = o, b = o ;
	a.size[axis] = plyt->drop_ctx.split - a.pos[axis] ;
	b.size[axis] = o.size[axis] - a.size[axis] ;
	b.pos[axis] += a.size[axis] ;
	// If a cell is inserted, it means it was removed from somewhere
	// else. The function musts determine how its previous space is
	// taken by neighbours.
	// #todo
	// Invalidate.
	plyt->drop_ctx.dirty = true ;
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
}

bool debug_is_target_junction(const plyt_t* plyt, axis_t axis, cid_t j) {
	junction_t* meuh = vec_get_ptr(&plyt->junctions[axis], j, junction_t) ;
	return (meuh->b == OUTER_EDGE) ;
}

#endif // PLYT_C
