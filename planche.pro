TEMPLATE = app
TARGET = qplanche
INCLUDEPATH += .
QT += widgets
CONFIG += debug

HEADERS += qt6/qplanche.hpp \
           qt6/qplanchehandle.hpp \
           src/planche.h
SOURCES += qt6/main.cpp \
           qt6/qplanche.cpp \
           qt6/qplanchehandle.cpp \
           src/planche.c
