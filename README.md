# Planche layout

A tiling layout for 2D (and 3D one day, maybe ?). What makes it particular is its flexibility:

- Written in C99, self-contained, copy-pastable anywhere.
- Not based on nested HBoxes and VBoxes ; cells can be extended/shrinked in both dimensions.

It is called planche because this kind of layout reminds me of comics layout (*planche de bande dessinée* in french). Also a tribute to [Ed, Edd 'n Eddy](https://ed.fandom.com/wiki/Plank)!

The repository also holds a **few adaptations to various UI frameworks**, and in particular GTK and swiftui. Those are exported as static libraries.

## SwiftUI

XCode can compile C, so the best (as in simple) strategy is to copy `planche.c`, `planche.h` and `vec.incl` as is in source, and carry on.

## GTK 4

Details of inheritance/composition/trait/voodoo-gobject will vary, as per gtk tradition of changing everything all the time. It was built by reading gtkpaned.c and gtkoverlay.c, so please expect unusual profanations of the common sense and good taste.

~~~
make gtk
~~~

## Qt 6

~~~
make qt
cd build
make
~~~

If the compilation fails due to some undefined link table error, just ensure you're compiling with clang :

~~~
cd build && rm -rf ./* && cd ..
export QMAKE_LINK=clang++ 
export QMAKE_C++=clang++ 
export QMAKE_C=clang
make qt
~~~
