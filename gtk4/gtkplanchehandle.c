#include "../src/planche.h"
#include "gtkplanchehandleprivate.h"

/// The handles are represented by implicit rectangles materialised
/// only by the 'contains' method. They are usually not drawn. They
/// could optionnaly vary (in thickness) over time, to accomodate
/// users precision.

G_DEFINE_TYPE(GtkPlancheHandle, gtk_planche_handle, GTK_TYPE_WIDGET);

// #define PLANCHE_DEBUG
#ifdef  PLANCHE_DEBUG
static
void gtk_planche_handle_snapshot(
	GtkWidget* widget,
	GtkSnapshot* snapshot
) {
	GtkPlancheHandle* handle = GTK_PLANCHE_HANDLE(widget) ;
	GtkWidget* parent = gtk_widget_get_parent(GTK_WIDGET(handle)) ;
	const int width  = gtk_widget_get_width(widget) ;
	const int height = gtk_widget_get_height(widget) ;
	if ((width > 0) && (height > 0)) {
		float meuh = ((float)handle->id) / 4.0 ;
		GdkRGBA rgba = {meuh, meuh, meuh, 1.0} ;
		// GdkRGBA rgba = {0.0, 0.8, 0.3, 1.0} ;
		graphene_rect_t bounds ;
		graphene_rect_init(&bounds, 0, 0, width, height) ;
		gtk_snapshot_append_color(snapshot, &rgba, &bounds) ;
	}
}
#endif

static
void gtk_planche_handle_class_init(GtkPlancheHandleClass* class) {
	GObjectClass* object_class = G_OBJECT_CLASS(class);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(class);
#ifdef PLANCHE_DEBUG
	widget_class->snapshot = gtk_planche_handle_snapshot ;
#endif
	gtk_widget_class_set_css_name(widget_class, "separator");
}

static
void gtk_planche_handle_drag_begin(
	GtkGestureDrag* self,
	gdouble x,
	gdouble y,
	gpointer user_data
) {
	GtkPlancheHandle* handle = GTK_PLANCHE_HANDLE(user_data) ;
	GtkWidget* widget = GTK_WIDGET(handle) ;
	GtkWidget* parent = gtk_widget_get_parent(widget) ;
	if (handle->type != GTK_PLANCHE_HANDLE_CORNER) {
		const axis_t a = (handle->type==GTK_PLANCHE_HANDLE_HORIZONTAL)?V:H;
		handle->callbacks->prepare_move(parent, a, handle->id) ;
	}
}

static
void gtk_planche_handle_drag_update(
	GtkGestureDrag* self,
	gdouble x,
	gdouble y,
	gpointer user_data
) {
	double drag_ox, drag_oy ;
	// BEWARE THIS IS THE OFFSET FROM THE (0, 0) OF THE HANDLE
	gtk_gesture_drag_get_start_point(self, &drag_ox, &drag_oy) ;
	// fprintf(stderr, "[gtk] Handle is dragged from (%f, %f).\n", drag_ox, drag_oy) ;
	// #fixme #current We provide an offset to update_move, whereas
	// an absolute position is required. Duh.
	// fprintf(stderr, "[gtk] Handle is dragged at (%f, %f).\n", x, y) ;
	GtkPlancheHandle* handle = GTK_PLANCHE_HANDLE(user_data) ;
	GtkWidget* widget = GTK_WIDGET(handle) ;
	GtkWidget* parent = gtk_widget_get_parent(widget) ;
	graphene_point_t zeros = {.x = 0.0, .y = 0.0} ;
	graphene_point_t orig ;
	// #fixme rename ^^^^ start_container ? at least stay coherent with next function
	bool fixme = gtk_widget_compute_point(widget, parent, &zeros, &orig) ;
	// if (! fixme) // wtf #fixme
	orig.x += drag_ox ;
	orig.y += drag_oy ;
	if (handle->type != GTK_PLANCHE_HANDLE_CORNER) {
		// #fixme double boolean...
		const axis_t a = (handle->type==GTK_PLANCHE_HANDLE_HORIZONTAL)?V:H;
		handle->callbacks->update_move(parent, a, (a==H) ? orig.x + x : orig.y + y) ;
	} else { // handle->type == GTK_PLANCHE_HANDLE_CORNER
		// A split is ongoing. Depending on the maximum delta, either
		// a vertical or horizontal split is proposed. At this point,
		// only the indicator widget should be updated.
	}
	// gtk_widget_queue_draw(GTK_WIDGET(parent)) ;
}

static
void gtk_planche_handle_drag_end(
	GtkGestureDrag* self,
	gdouble x,
	gdouble y,
	gpointer user_data
) {
	GtkPlancheHandle* handle = GTK_PLANCHE_HANDLE(user_data) ;
	GtkWidget* widget = GTK_WIDGET(handle) ;
	GtkWidget* parent = gtk_widget_get_parent(widget) ;
	gdouble startx, starty ;
	gtk_gesture_drag_get_start_point(self, &startx, &starty) ;
	graphene_point_t start_handle = {.x = startx, .y = starty} ;
	graphene_point_t start_container ;
	const bool transformed = gtk_widget_compute_point(
		widget, parent,	&start_handle, &start_container) ;
	// Ok so apparently in the GNOME world a linear transform can
	// actually fail. Maybe if one widget is missing a frame ?
	if (! transformed) {
		exit(12) ; // This **cannot** happen. #fixme #error
	}
	// GtkLayoutManager* mgr = gtk_widget_get_layout_manager(parent) ;
	// GtkPlancheLayout* layout = GTK_PLANCHE_LAYOUT(mgr) ;
	// plyt_t* plyt = gtk_planche_layout_plyt(layout) ; // #
	if (handle->type != GTK_PLANCHE_HANDLE_CORNER) {
		const axis_t a = (handle->type==GTK_PLANCHE_HANDLE_HORIZONTAL)?V:H;
		handle->callbacks->commit_move(parent, a, (a==H) ? start_container.x + x : start_container.y + y) ;
	} else { // handle->type == GTK_PLANCHE_HANDLE_CORNER
		// A split has to be done. Depending on the maximum delta,
		// it is either a vertical or horizontal split. Notice that
		// the start point has to be transformed into planche frame.
		axis_t axis ;
		pixels_t lim ;
		if (fabs(x) > fabs(y)) {
			axis = H ;
			lim = start_container.x + x ;
		} else {
			axis = V ;
			lim = start_container.y + y ;
		}
		handle->callbacks->split(parent, axis, handle->id, lim) ;
	}
	// gtk_widget_queue_draw(GTK_WIDGET(parent)) ;
}

static
void gtk_planche_handle_init(GtkPlancheHandle* handle) {
	// Make it expandable.
    gtk_widget_set_vexpand(GTK_WIDGET(handle), true) ;
    gtk_widget_set_hexpand(GTK_WIDGET(handle), true) ;
#ifndef PLANCHE_DEBUG
    gtk_widget_set_opacity(GTK_WIDGET(handle), 0.0) ;
#endif
	// // Split gesture.
	// GtkGesture* split = gtk_gesture_click_new() ;
	// gtk_gesture_single_set_button(GTK_GESTURE_SINGLE(split), 3) ;
	// gtk_widget_add_controller(GTK_WIDGET(handle), GTK_EVENT_CONTROLLER(split)) ;
 	// g_signal_connect(split,"released", 	  G_CALLBACK(gtk_planche_handle_split), 	   handle) ;
    // Dragging gesture.
    GtkGesture* drag = gtk_gesture_drag_new() ;
	gtk_widget_add_controller(GTK_WIDGET(handle), GTK_EVENT_CONTROLLER(drag)) ;
    g_signal_connect(drag, "drag-begin",  G_CALLBACK(gtk_planche_handle_drag_begin),  handle) ;
    g_signal_connect(drag, "drag-update", G_CALLBACK(gtk_planche_handle_drag_update), handle) ;
    g_signal_connect(drag, "drag-end",    G_CALLBACK(gtk_planche_handle_drag_end),    handle) ;
    // Cursors.
    // #todo ns-resize, ew-resize.
    GdkCursor* def = gtk_widget_get_cursor(GTK_WIDGET(handle)) ;
    GdkCursor* cursor = gdk_cursor_new_from_name("cell", def) ;
    gtk_widget_set_cursor(GTK_WIDGET(handle), cursor) ;
}

GtkWidget* gtk_planche_handle_new(void) {
	return g_object_new(GTK_TYPE_PLANCHE_HANDLE, NULL) ;
}