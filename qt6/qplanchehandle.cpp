#include "qplanchehandle.hpp"

#include <QPainter>

#include <iostream>

QPlancheHandle::QPlancheHandle(QWidget* parent)
	: QAbstractButton(parent)
{
	setGeometry(QRect(0, 0, 100, 100)) ;
}

bool QPlancheHandle::event(QEvent* ev) {
	return QAbstractButton::event(ev) ;
}

void QPlancheHandle::paintEvent(QPaintEvent*) {
#define PLANCHE_DEBUG
#ifdef  PLANCHE_DEBUG
	QPainter p(this) ;
	QBrush solid(QColor(0, 255, 0, 255)) ;
	p.fillRect(QRect(5, 5, 105, 105), solid) ;
#endif
}

void QPlancheHandle::keyPressEvent(QKeyEvent* ev) {
	std::cout << "key press event" << std::endl ;
	return QAbstractButton::keyPressEvent(ev) ;
}

void QPlancheHandle::mouseMoveEvent(QMouseEvent* ev) {
	std::cout << "mouse move event (" << (void*) this << ")" << std::endl ;
	return QAbstractButton::mouseMoveEvent(ev) ;
}

bool QPlancheHandle::hitButton(const QPoint& pos) const {
	return
		(pos.x() >= 5)  and
		(pos.x() < 105) and
		(pos.y() >= 5)  and
		(pos.y() < 105) ;
	// QStyleOptionButton option;
 //    initStyleOption(&option);
 //    const QRect bevel = style()->subElementRect(QStyle::SE_PushButtonBevel, &option, this);
 //    return bevel.contains(pos);
}

QSize QPlancheHandle::sizeHint() const {
	return QSize(100, 100) ;
}

QSize QPlancheHandle::minimumSizeHint() const {
	return sizeHint() ;
}
