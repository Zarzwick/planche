#ifndef PLYT_H
#define PLYT_H

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

enum axis_t { H = 0, V = 1 } ;
enum xyz_t  { X = 0, Y = 1, Z = 2 } ;

#if (H != X) || (V != Y)
#error "Inconsistent constants."
#endif

typedef int cid_t ;
typedef int pixels_t ;
typedef pixels_t fixed_point_t ;
typedef enum xyz_t xyz_t ;
typedef enum axis_t axis_t ;
typedef struct junction_t junction_t ;
typedef struct interval_t interval_t ;
typedef unsigned int uint ;

typedef struct plyt_pixels_rectangle {
	pixels_t pos[2] ;
	pixels_t size[2] ;
} plyt_pixels_rectangle ;

typedef struct plyt_unit_rectangle {
	double pos[2] ;
	double size[2] ;
} plyt_unit_rectangle ;

///
/// Central type of the api.
///
typedef struct plyt_t plyt_t ;

plyt_t* plyt_init(void) ;

void plyt_deinit(plyt_t* plyt) ;

void plyt_add_junction(
	plyt_t* plyt,
	axis_t axis,
	cid_t a,
	cid_t b,
	fixed_point_t p,
	fixed_point_t s
) ;

void plyt_recompute_junctions(plyt_t* plyt) ;

void plyt_recompute_sizes(plyt_t* plyt) ;

void plyt_resize(plyt_t* plyt, size_t w, size_t h) ;

void plyt_split(
	plyt_t* plyt,
	axis_t axis,
	cid_t cell,
	pixels_t pixels
) ;

void plyt_merge(
	plyt_t* plyt,
	axis_t axis,
	cid_t j,
	bool a_into_b
) ;

bool plyt_prepare_move(
	plyt_t* plyt,
	axis_t axis,
	cid_t j
) ;

pixels_t plyt_control_move_and_suggest(
	plyt_t* plyt,
	axis_t axis,
	pixels_t p
) ;

void plyt_move(
	plyt_t* plyt,
	axis_t axis,
	pixels_t p
) ;

bool debug_is_target_junction(const plyt_t* plyt, axis_t axis, cid_t j) ;

size_t plyt_cells_count(const plyt_t* plyt) ;

size_t plyt_junctions_count(const plyt_t* plyt, axis_t axis) ;

plyt_pixels_rectangle plyt_abs_cell(const plyt_t* plyt, cid_t c) ;

plyt_pixels_rectangle plyt_abs_junction(const plyt_t* plyt, axis_t axis, int j) ;

plyt_unit_rectangle plyt_rel_cell(const plyt_t* plyt, cid_t c) ;

// plyt_unit_rectangle plyt_rel_junction(const plyt_t* plyt, axis_t axis, int j) ;

///
/// Such a function should fill mins with the minimum size of cell i,
/// and defs with the default size of cell i.
///
typedef void (*fetch_op)(int i, pixels_t* mins, pixels_t* defs, void*) ;

///
/// An operator passed to a fold. Usually this will be min or max.
///
typedef pixels_t (*fold_op)(pixels_t a, pixels_t b, void* userdata) ;

///
/// Computes the minimum and default sizes of the container, based on
/// the corresponding sizes of each child widget. The op is used as
/// the operator of a left fold, over the (implicit) list of defaults
/// sizes accumulated over the branches. It may sounds confusing, and
/// the doc [here](perdu.com) is made for that.
///
void plyt_min_and_default_sizes(
	plyt_t* plyt,
	axis_t axis,
	pixels_t* mins,
	pixels_t* defs,
	fetch_op fetch,
	void* fetch_data,
	fold_op fold,
	void* fold_data
) ;

#ifdef __cplusplus
}
#endif

#endif // PLYT_H
