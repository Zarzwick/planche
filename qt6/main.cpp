#include "qplanche.hpp"
#include "qplanchehandle.hpp" // #fixme #remove

#include <iostream>

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QApplication>

// Meh.
// export QMAKE_CC=clang
// export QMAKE_CXX=clang++
// export QMAKE_LINK=clang++

// #note A simplex implementation exists in Qt:
// https://github.com/qt/qtbase/blob/dev/src/widgets/graphicsview/qsimplex_p.h

int main(int argc, char* argv[]) {
    QApplication app(argc, argv) ;
    QWidget window ;
    QPlanche* layout = new QPlanche(&window) ;
    QPushButton* button_a = new QPushButton("A") ;
    QPushButton* button_b = new QPushButton("B") ;
    QPushButton* button_c = new QPushButton("C") ;
    // QPushButton* button_d = new QPushButton("D") ;
    // QPushButton* button_e = new QPushButton("E") ;
    // QPlancheHandle* handle = new QPlancheHandle() ;
    // QHBoxLayout* layout = new QHBoxLayout(&window) ;
    layout->addWidget(button_a) ;
    layout->addWidget(button_b) ;
    layout->addWidget(button_c) ;
    // layout->addWidget(button_d) ;
    // layout->addWidget(button_e) ;
    // layout->addWidget(handle) ;
    window.resize(1000, 500) ;
    window.show() ;
    window.setWindowTitle(QApplication::translate("toplevel", "Planche Example")) ;
	return app.exec() ;
}
