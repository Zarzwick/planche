#pragma once

#include "../src/planche.h"

#include <QList>
#include <QRect>
#include <QLayout>
#include <QLayoutItem>

///
/// ...
///
class QPlanche: public QLayout {
	Q_OBJECT

public:
	explicit QPlanche(QWidget* parent=nullptr) ;
	virtual ~QPlanche() ;

	QSize sizeHint() const override ;
	QSize minimumSize() const override ;
	QSize maximumSize() const override ;

	void addWidget(QWidget* widget) ;
	void addItem(QLayoutItem* item) override ;
	bool isEmpty() const override ;
	int count() const override ;

	QLayoutItem* itemAt(int index) const override ;
	QLayoutItem* takeAt(int index) override ;

	void setGeometry(const QRect& r) override ;

private:
	void recomputeMinAndDefSizes() ;
	
	void clearHandles() ;
	void clearCornerHandles() ;
	void clearJunctionHandles() ;

	void resizeHandles() ;
	void resizeCornerHandles() ;
	void resizeJunctionHandles() ;

	void addHandles() ;
	void addCornerHandles() ;
	void addJunctionHandles() ;

private:
	QList<QLayoutItem*> items ;
	plyt_t* plyt ;
	pixels_t min_size_x ;
	pixels_t min_size_y ;
	pixels_t def_size_x ;
	pixels_t def_size_y ;
} ;
