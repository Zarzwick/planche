#include "gtkplanche.h"

#include "../src/planche.h"
#include "gtkplanchelayout.h"
#include "gtkplanchehandleprivate.h"

typedef struct _GtkPlancheClass GtkPlancheClass ;
typedef struct _GtkPlancheHandleCallbacks GtkPlancheHandleCallbacks ;

#define TEMP_W 1000
#define TEMP_H 600

/// The main container widget provided by this library.
///
/// It stores its children in a precise layout. For a number of cells
/// nc, the nc first chilren are actual widgets, the nc next are
/// corners handles, and the remaining ones are junction handles.
struct _GtkPlanche {
	GtkWidget parent_instance ;
	GtkPlancheHandleCallbacks callbacks ;

	/// This is used as a widget provider when a new cell is spawned.
	GtkWidget* pending_widget ;

	/// This is used for the base case, where a cell exists, but no
	/// widget is present in the children list yet.
	bool empty_cell ;
} ;

struct _GtkPlancheClass {
	GtkWidgetClass parent_class ;
} ;

static void gtk_planche_init(GtkPlanche*) ;
static void gtk_planche_dispose(GObject*) ;
static void gtk_planche_class_init(GtkPlancheClass*) ;
static void gtk_planche_buildable_iface_init(GtkBuildableIface*) ;
static gboolean gtk_planche_get_child_position(GtkOverlay*, GtkWidget*, GtkAllocation*, gpointer) ;

G_DEFINE_TYPE_WITH_CODE(
	GtkPlanche, gtk_planche, GTK_TYPE_WIDGET,
	G_IMPLEMENT_INTERFACE(GTK_TYPE_BUILDABLE, gtk_planche_buildable_iface_init)
)

GtkWidget* gtk_planche_new(void) {
	return g_object_new(GTK_TYPE_PLANCHE, NULL) ;
}

static inline
GtkPlancheLayout* gtk_planche_get_planche_layout(GtkPlanche* planche) {
	GtkLayoutManager* m = gtk_widget_get_layout_manager(GTK_WIDGET(planche)) ;
	return GTK_PLANCHE_LAYOUT(m) ;
}

static
void gtk_planche_clear_handles(GtkPlanche* planche) {
	if (planche->empty_cell)
		return ;
	const GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	const plyt_t* plyt = gtk_planche_layout_const_plyt(layout) ;
	const uint njh = plyt_junctions_count(plyt, H) ;
	const uint njv = plyt_junctions_count(plyt, V) ;
	const uint nc = plyt_cells_count(plyt) ;
	const uint n = njh + njv + nc ;
	// const uint n = nc ;
	GtkWidget* curr = gtk_widget_get_last_child(GTK_WIDGET(planche)) ;
    for (uint i = 0 ; i < n ; i ++) {
    	GtkWidget* safe = curr ;
    	curr = gtk_widget_get_prev_sibling(safe) ;
    	gtk_widget_unparent(safe) ;
    	// gtk_widget_destroy(safe) ; // #fixme #leak
    }
}

static
void gtk_planche_add_handles(GtkPlanche* planche) {
	if (planche->empty_cell)
		return ;
	const GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	const plyt_t* plyt = gtk_planche_layout_const_plyt(layout) ;
	const uint njh = plyt_junctions_count(plyt, H) ;
	const uint njv = plyt_junctions_count(plyt, V) ;
	const uint nc = plyt_cells_count(plyt) ;
	// const uint n = njh + njv + nc ;
	for (uint i = 0; i < nc; ++ i) {
		GtkWidget* widget = gtk_planche_handle_new() ;
		GtkPlancheHandle* handle = GTK_PLANCHE_HANDLE(widget) ;
		handle->id = i ;
		handle->type = GTK_PLANCHE_HANDLE_CORNER ;
		handle->callbacks = &planche->callbacks ;
		gtk_widget_set_parent(widget, GTK_WIDGET(planche)) ;
	}
	for (uint i = 0; i < njh; ++ i) {
		GtkWidget* widget = gtk_planche_handle_new() ;
		GtkPlancheHandle* handle = GTK_PLANCHE_HANDLE(widget) ;
		handle->id = i ;
		handle->type = GTK_PLANCHE_HANDLE_HORIZONTAL ;
		handle->callbacks = &planche->callbacks ;
		gtk_widget_set_parent(widget, GTK_WIDGET(planche)) ;
	}
	for (uint i = 0; i < njv; ++ i) {
		GtkWidget* widget = gtk_planche_handle_new() ;
		GtkPlancheHandle* handle = GTK_PLANCHE_HANDLE(widget) ;
		handle->id = i ;
		handle->type = GTK_PLANCHE_HANDLE_VERTICAL ;
		handle->callbacks = &planche->callbacks ;
		gtk_widget_set_parent(widget, GTK_WIDGET(planche)) ;
	}
}

// typedef struct {
// 	pixels_t* mins ;
// 	pixels_t* defs ;
// } arrays_t ;

// static
// void from_arrays(int i, pixels_t* mins, pixels_t* defs, void* data) {
// 	arrays_t* arrays = (arrays_t*) data ;
// 	*mins = arrays->mins[i] ;
// 	*defs = arrays->defs[i] ;
// }

// static
// pixels_t maxp(pixels_t a, pixels_t b, void*) {
// 	return (a < b) ? b : a ;
// }

static
void gtk_recompute_min_and_def_sizes(GtkPlanche* planche) {
	// // The first loop is just about getting min. and default sizes of
	// // all the children.
	// const uint nc = plyt_cells_count(planche->layout) ;
	// pixels_t* hmins = malloc(nc * sizeof(pixels_t)) ;
	// pixels_t* hdefs = malloc(nc * sizeof(pixels_t)) ;
	// pixels_t* vmins = malloc(nc * sizeof(pixels_t)) ;
	// pixels_t* vdefs = malloc(nc * sizeof(pixels_t)) ;
	// for (uint i = 0u; i < nc; ++ i) {
	// 	// const pixels_t m = 30 ; // items[i]->minimumSize() ;
	// 	// const pixels_t d = 30 ; // items[i]->sizeHint() ;
	// 	// hmins[i] = m.width() ;
	// 	// hdefs[i] = d.width() ;
	// 	// vmins[i] = m.height() ;
	// 	// vdefs[i] = d.height() ;
	// }
	// // Then we compute the container geometry, in both directions.
	// arrays_t arrs = {hmins, hdefs} ;
	// plyt_min_and_default_sizes(
	// 	planche->layout, H,
	// 	&planche->min_size_x,
	// 	&planche->def_size_x,
	// 	from_arrays, &arrs,
	// 	maxp, NULL
	// ) ;
	// arrs = (arrays_t) {vmins, vdefs} ;
	// plyt_min_and_default_sizes(
	// 	planche->layout, V,
	// 	&planche->min_size_y,
	// 	&planche->def_size_y,
	// 	from_arrays, &arrs,
	// 	maxp, NULL
	// ) ;
}

// This depends on pending_widget. If it is non-null, it will be
// added as the new widget. Otherwise, a new default widget is
// produced.
static
void gtk_planche_split(GtkWidget* widget, axis_t axis, cid_t cell, pixels_t lim) {
	GtkPlanche* planche = GTK_PLANCHE(widget) ;
	GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	plyt_t* plyt = gtk_planche_layout_plyt(layout) ;
	// Clear handles.
	gtk_planche_clear_handles(planche) ;
	// Modify the layout.
	plyt_split(plyt, axis, cell, lim) ;
	plyt_recompute_junctions(plyt) ;
	plyt_recompute_sizes(plyt) ;
	// Provide a new widget if required.
	if (! planche->pending_widget) {
		planche->pending_widget = gtk_button_new_with_label("meuh") ;
	}
	// Affect the children widgets. The empty_cell is guaranteed to be false.
	gtk_widget_set_parent(planche->pending_widget, GTK_WIDGET(planche)) ;
	gtk_planche_layout_update_counters(layout) ;
	gtk_planche_add_handles(planche) ;
	planche->pending_widget = NULL ;
	gtk_widget_queue_allocate(widget) ;
}

static
bool gtk_planche_prepare_move(GtkWidget* widget, axis_t axis, cid_t j) {
	GtkPlanche* planche = GTK_PLANCHE(widget) ;
	GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	plyt_t* plyt = gtk_planche_layout_plyt(layout) ;
	return plyt_prepare_move(plyt, axis, j) ;
}

static
void gtk_planche_update_move(GtkWidget* widget, axis_t axis, pixels_t p) {
	fprintf(stderr, "[gtk] Move position is proposed as %d.\n", p) ;
	GtkPlanche* planche = GTK_PLANCHE(widget) ;
	GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	plyt_t* plyt = gtk_planche_layout_plyt(layout) ;
	pixels_t ctrl = plyt_control_move_and_suggest(plyt, axis, p) ;
	if (ctrl != p) { // #fixme
		fprintf(stderr, "[gtk] Move position is suggested or clamped.\n") ;
	}
}

static
void gtk_planche_commit_move(GtkWidget* widget, axis_t axis, pixels_t p) {
	GtkPlanche* planche = GTK_PLANCHE(widget) ;
	GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	plyt_t* plyt = gtk_planche_layout_plyt(layout) ;
	pixels_t ctrl = plyt_control_move_and_suggest(plyt, axis, p) ;
	plyt_move(plyt, axis, ctrl) ;
	plyt_recompute_sizes(plyt) ;
	plyt_recompute_junctions(plyt) ;
	gtk_widget_queue_allocate(widget) ;
}

static
void gtk_planche_init(GtkPlanche* planche) {
	gtk_widget_set_focusable(GTK_WIDGET(planche), true) ;
	gtk_widget_set_overflow(GTK_WIDGET(planche), GTK_OVERFLOW_HIDDEN) ;
	// Populate handles callbacks.
	planche->callbacks.split = gtk_planche_split ;
	planche->callbacks.prepare_move = gtk_planche_prepare_move ;
	planche->callbacks.update_move = gtk_planche_update_move ;
	planche->callbacks.commit_move = gtk_planche_commit_move ;
	// This is for the initial case where a cell exists, but no child
	// is present yet to occupy it.
	planche->empty_cell = true ;
}

static
void gtk_planche_class_init(GtkPlancheClass* class) {
	GObjectClass* object_class = G_OBJECT_CLASS(class);
	GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(class);
	object_class->dispose = gtk_planche_dispose ;
	gtk_widget_class_set_css_name(widget_class, "planche") ;
	gtk_widget_class_set_layout_manager_type(widget_class, GTK_TYPE_PLANCHE_LAYOUT) ;
}

// uint meh = 800 ;

static
void gtk_planche_default_host_cell(
	GtkPlanche* planche,
	cid_t* host,
	pixels_t* lim
) {
	*host = 0 ;
	GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	const plyt_t* plyt = gtk_planche_layout_const_plyt(layout) ;
	const plyt_pixels_rectangle r = plyt_abs_cell(plyt, *host) ;
	*lim = r.pos[X] + r.size[X] / 2 ;
}

GtkWidget* gtk_planche_add_child(
	GtkPlanche* planche,
	GtkWidget* child
) {
	// Affect the layout.
	GtkPlancheLayout* layout = gtk_planche_get_planche_layout(planche) ;
	plyt_t* plyt = gtk_planche_layout_plyt(layout) ;
	// assert(planche->pending_widget == NULL) ;
	planche->pending_widget = child ;
	if (! planche->empty_cell) {
		cid_t cell ;
		pixels_t l ;
		gtk_planche_default_host_cell(planche, &cell, &l) ;
		gtk_planche_split(GTK_WIDGET(planche), H, cell, l) ;
	} else {
		planche->empty_cell = false ;
		gtk_widget_set_parent(child, GTK_WIDGET(planche)) ;
		gtk_planche_add_handles(planche) ;
		planche->pending_widget = NULL ;
	}
}

static
void gtk_planche_dispose(GObject* object) {
	GtkPlanche* planche = GTK_PLANCHE(object);
	GtkWidget* child;
	// g_clear_pointer(&planche->child, gtk_widget_unparent);
	while ((child = gtk_widget_get_first_child(GTK_WIDGET(planche))))
		gtk_widget_unparent(child);
	G_OBJECT_CLASS(gtk_planche_parent_class)->dispose(object);
}

static GtkBuildableIface* parent_buildable_iface ;

static
void gtk_planche_buildable_add_child(
	GtkBuildable* 	buildable,
	GtkBuilder*   	builder,
	GObject*      	child,
	const char* 	type
) {
	if (GTK_IS_WIDGET(child)) {
		gtk_planche_add_child(GTK_PLANCHE(buildable), GTK_WIDGET(child)) ;
	} else {
		parent_buildable_iface->add_child(buildable, builder, child, type) ;
	}
}

static
void gtk_planche_buildable_iface_init(GtkBuildableIface* iface) {
	parent_buildable_iface = g_type_interface_peek_parent(iface) ;
	iface->add_child = gtk_planche_buildable_add_child ;
}
